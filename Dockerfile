FROM anapsix/alpine-java

LABEL authors="Jose A. Rodriguez <joseanibal.rod@gmail.com>"

ADD target/imperio-*.jar /app/imperio.jar
ADD imperio /opt/imperio/

WORKDIR /app

run chmod 777 /opt/imperio/bin/start.sh

ENTRYPOINT /opt/imperio/bin/start.sh