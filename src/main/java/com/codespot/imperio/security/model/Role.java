package com.codespot.imperio.security.model;

import lombok.Data;

import javax.persistence.*;

@Entity(name = "ROLE")
@Data
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ROLE_ID")
    private Long id;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "STATUS")
    private Short status;
}
