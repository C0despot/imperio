package com.codespot.imperio.security.model;

import com.codespot.imperio.model.entity.util.Status;
import com.codespot.imperio.model.enums.StatusType;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Data
@Entity
@Table(name = "USER")
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "USER_ID")
    private Long userId;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @Column(name = "EMAIL_ADDRESS")
    private String email;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "AGE")
    private Long age;

    @Column(name = "BIRTH_DATE")
    private Date birthDate;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "VALIDATED")
    private Boolean validated;

    @Column(name = "BLOOD_TYPE")
    private String bloodType;

    @Column(name = "BANK_ACCOUNT_NUMBER")
    private String bankAccountNumber;

    @Column(name = "STATUS")
    @Enumerated(value = EnumType.STRING)
    private StatusType.User status;

    @Column(name = "LOCKED")
    private Boolean locked;

    @ManyToOne
    @JoinColumn(name = "ROLE")
    private Role role;

    @Column(name = "JOIN_DATE")
    private Date joinDate;

    @Column(name = "FIRED_DATE")
    private Date fireDate;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @ManyToOne
    @JoinColumn(name = "CREATED_BY")
    private User createdBy;

    @ManyToOne
    @JoinColumn(name = "MODIFIED_BY")
    private User modifiedBy;

    @Column(name = "INITIAL_SALARY")
    private String initialSalary;

    @Column(name = "CURRENT_SALARY")
    private String currentSalary;

    @Column(name = "ID_CARD")
    private String idCard;

    @Column(name = "DRIVER_PERMIT")
    private Boolean driverPermit;

    @Column(name = "MARRIED")
    private String married;

    @Column(name = "CHILDREN")
    private Integer children;

    @Column(name = "WIFE_NAME")
    private String wife;

    @Column(name = "NATIONALITY")
    private String nationality;

    @Column(name = "GENDER")
    private String gender;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return validated;
    }
}
