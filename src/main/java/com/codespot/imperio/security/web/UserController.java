package com.codespot.imperio.security.web;


import com.codespot.imperio.security.model.User;
import com.codespot.imperio.security.repository.RoleRepository;
import com.codespot.imperio.security.service.SecurityService;
import com.codespot.imperio.security.service.UserService;
import com.codespot.imperio.security.validator.UserValidator;
import com.codespot.imperio.service.mail.MailService;
import com.codespot.imperio.util.SessionUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;

import javax.mail.MessagingException;
import java.security.GeneralSecurityException;
import java.security.Principal;

@Controller
@RequestMapping(value = "user")
public class UserController {

    private final
    UserService userService;

    private final
    SecurityService securityService;

    private final
    UserValidator userValidator;

    private final
    MailService mailService;

    private final
    RoleRepository roleRepository;

    private final
    ModelMapper modelMapper;

    @Autowired
    public UserController(UserService userService, SecurityService securityService, UserValidator userValidator, MailService mailService, RoleRepository roleRepository, ModelMapper modelMapper) {
        this.userService = userService;
        this.securityService = securityService;
        this.userValidator = userValidator;
        this.mailService = mailService;
        this.roleRepository = roleRepository;
        this.modelMapper = modelMapper;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout, Principal principal) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "login";
    }
    @GetMapping(value = {"", "/"})
    private ResponseEntity welcome(Principal principal) {
        if (principal == null)
            return ResponseEntity.ok("NO_OK");
        SessionUtil session = modelMapper.map(RequestContextHolder.currentRequestAttributes().getSessionMutex(), SessionUtil.class);
        return ResponseEntity.ok(session);
    }

    @PostMapping(value = {"", "/"})
    public ResponseEntity saveUser(@RequestBody User user, Principal principal) {
        user = this.userService.save(user, "anibalrljose");
        if (user == null)
            return ResponseEntity.unprocessableEntity().build();
        return ResponseEntity.ok(user);
    }

    @PostMapping(value = "one")
    @ResponseBody
    public ResponseEntity getOne(@RequestBody String username) throws MessagingException, GeneralSecurityException {
        User user = this.userService.findByUsername(username);
        if (user == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(user);
    }

    @GetMapping(value = "roles")
    public ResponseEntity getRoles() {
        return ResponseEntity.ok(this.roleRepository.findAll());
    }

    @PostMapping(value = "token")
    public ResponseEntity getUserByToken(@RequestBody String token) {
        return ResponseEntity.ok(this.userService.getByToken(token));
    }

    @PostMapping(value = "validate")
    public ResponseEntity validate(@RequestBody User user) {
        return ResponseEntity.ok(this.userService.validateUser(user));
    }

    @GetMapping(value = "me")
    public ResponseEntity me(Principal principal) {
        User user = this.userService.findByUsername(principal.getName());
        if (user == null)
            return ResponseEntity.unprocessableEntity().build();
        return ResponseEntity.ok(user);
    }
}


