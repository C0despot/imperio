package com.codespot.imperio.security.repository;

import com.codespot.imperio.security.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
