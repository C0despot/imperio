package com.codespot.imperio.security.repository;


import com.codespot.imperio.security.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Long> {
    @Query(value = "SELECT * FROM USER WHERE USERNAME = :username OR EMAIL_ADDRESS = :username", nativeQuery = true)
    User findUser(@Param("username") String username);

    User findByPasswordEquals(String password);

    Page<User>findAllByUsernameContainingOrEmailContainingOrFirstNameContainingOrLastNameContaining(String p1, String p2, String p3, String p4, Pageable p5);
    Page<User>findAllByUsernameContainingOrEmailContainingOrFirstNameContainingOrLastNameContainingOrUserId(String p1, String p2, String p3, String p4, Long p5, Pageable p6);
}
