package com.codespot.imperio.security.service;

import com.codespot.imperio.security.model.User;
import com.codespot.imperio.security.repository.UserRepository;
import com.codespot.imperio.service.mail.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    private final
    UserRepository userRepository;

    private final
    MailService mailService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, MailService mailService) {
        this.userRepository = userRepository;
        this.mailService = mailService;
    }

    @Override
    @Transactional
    public User save(User user, String username) {
        User author = this.userRepository.findUser(username);
        if (username == null)
            return null;
        if (user.getUserId() == null) {
            user.setPassword(UUID.randomUUID().toString().replace("-", ""));
            user.setValidated(false);
            user.setModifiedBy(author);
            user.setCreatedBy(author);
            user.setCreatedDate(new Date());
            user.setModifiedDate(new Date());
            user = this.userRepository.save(user);
            try {
                this.mailService.sendWelcomeEmail(user);
            }catch (Exception e){
                e.printStackTrace();
            }

        } else {
            if (!user.getUserId().equals(author.getUserId())) {
                user.setModifiedBy(author);
                user.setModifiedDate(new Date());
                user = this.userRepository.save(user);
            }
        }
        return user;
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findUser(username);
    }

    @Override
    public User getByToken(String token) {
        User user = this.userRepository.findByPasswordEquals(token);
        user.setPassword(null);
        return user;
    }

    @Override
    public User validateUser(User user) {
        user.setValidated(true);
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        user.setPassword(encoder.encode(user.getPassword()));
        user = this.userRepository.save(user);
        return user;
    }
}
