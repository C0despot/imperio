package com.codespot.imperio.security.service;

import com.codespot.imperio.security.model.User;

public interface UserService {
    User save(User user, String username);
    User findByUsername(String username);
    User getByToken(String token);
    User validateUser(User user);
}
