package com.codespot.imperio.model.entity;

import com.codespot.imperio.model.enums.VoucherType;
import com.codespot.imperio.service.sales.SalesService;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "COMPANY_VOUCHER")
public class CompanyVoucher {

    @Id
    @Column(name = "ID")
    @Enumerated(EnumType.STRING)
    private SalesService.ReportOrigin id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "FINAL_CONSUMER")
    private String finalConsumer;

    @Column(name = "GOVERNMENTAL")
    private String governmental;

    @Column(name = "FISCAL_CREDIT")
    private String fiscalCredit;

    @Column(name = "CREDIT_NOTE")
    private String creditNote;

    public String next(Short voucherType) {
        VoucherType type = VoucherType.get(voucherType);
        switch (type) {
            case CREDIT_NOTE:
                this.creditNote = base8(next(this.creditNote));
                return this.creditNote;
            case GOVERNMENTAL:
                this.governmental = base8(next(this.governmental));
                return this.governmental;
            case FINAL_CONSUMER:
                this.finalConsumer = base8(next(this.finalConsumer));
                return this.finalConsumer;
            case FISCAL_CREDIT:
                this.fiscalCredit = base8(next(this.fiscalCredit));
                return this.fiscalCredit;
        }
        return "...";
    }

    private String base8(Integer sequence) {
        return String.format("%08d", sequence);
    }

    private Integer next(String value) {
        return (Integer.valueOf(value) + 1);
    }
}
