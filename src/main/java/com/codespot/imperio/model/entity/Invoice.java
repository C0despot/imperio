package com.codespot.imperio.model.entity;

import com.codespot.imperio.model.enums.StatusType;
import com.codespot.imperio.security.model.User;
import com.codespot.imperio.service.sales.SalesService;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "INVOICE")
public class Invoice {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "INVOICE_ID")
    private Long invoiceId;

    @Column(name = "CURRENT_TAX")
    private Double tax;

    @Column(name = "TOTAL_AMOUNT")
    private String total;

    @Column(name = "SUBTOTAL_AMOUNT")
    private String subTotal;

    @Column(name = "TOTAL_DISCOUNT")
    private String discountTtl;

    @Column(name = "DISCOUNT_PERCENTAGE")
    private String discountPct;

    @ManyToOne
    @JoinColumn(name = "CUSTOMER", nullable = false)
    private Customer customer;

    @Column(name = "DATE")
    private Date date;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @ManyToOne
    @JoinColumn(name = "MODIFIED_BY")
    private User modifiedBy;

    @ManyToOne
    @JoinColumn(name = "CREATED_BY")
    private User createdBy;

    @Column(name = "STATUS")
    @Enumerated(value = EnumType.STRING)
    private StatusType.Invoice status;

    @Column(name = "VOUCHER")
    private Short voucherType;

    @Column(name = "VOUCHER_NO")
    private String voucherNo;

    @Column(name = "BALANCE")
    private String balance;

    @Column(name = "CREDIT")
    private String credit;

    @ManyToOne
    @JoinColumn(name = "SQRT_DOC")
    private CashBox cashBox;

    @Column(name = "PAYMENT_METHOD")
    private Integer payment;

    @Column(name = "ORDER_NO")
    private String orderNo;

    @Column(name = "PAYMENT_CONDITION")
    private String paymentCondition;

    @Column(name = "VALIDITY")
    private String validity;

    @Column(name = "SHIFT_TIME")
    private String shiftTime;

    @Column(name = "PROCEDURE_REF")
    private String procedureRef;

    @Enumerated(EnumType.STRING)
    @Column(name = "COMPANY")
    private SalesService.ReportOrigin company;

    public String getOrderNo() {
        return orderNo == null ? null : orderNo.toUpperCase();
    }

    public SalesService.ReportOrigin getCompany() {
        if (this.company == null)
            this.company = SalesService.ReportOrigin.RVIMP;
        return company;
    }
}
