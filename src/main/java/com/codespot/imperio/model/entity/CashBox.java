package com.codespot.imperio.model.entity;

import com.codespot.imperio.model.enums.StatusType;
import com.codespot.imperio.security.model.User;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "CASH_BOX")
public class CashBox {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "DOC_ID")
    private Long docId;

    @Column(name = "TOTAL_CASH")
    private String totalCash;

    @Column(name = "TOTAL_CREDIT")
    private String totalCredit;

    @Column(name = "TOTAL_CREDIT_CARD")
    private String totalCreditCard;

    @Column(name = "TOTAL_CHECK")
    private String totalCheck;

    @Column(name = "TATAL_AMOUNT")
    private String total;

    @Column(name = "TOTAL_TAX")
    private String tax;

    @Column(name = "TOTAL_GAIN")
    private String gain;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @ManyToOne
    @JoinColumn(name = "CREATED_BY")
    private User createdBy;

    @Column(name = "START_DAY_TOTAL_CASH")
    private Double startCash;

    @Column(name = "END_DAY_TOTAL_CASH")
    private Double endCash;

    @Column(name = "STATUS")
    @Enumerated(value = EnumType.STRING)
    private StatusType.Sale status;

    @ManyToOne
    @JoinColumn(name = "OPEN_CASH")
    private OpenCashBox openCashBox;

    @ManyToOne
    @JoinColumn(name = "CLOSE_CASH")
    private OpenCashBox closeCashBox;
}