package com.codespot.imperio.model.entity;

import com.codespot.imperio.model.entity.util.Status;
import com.codespot.imperio.model.entity.util.Vendor;
import com.codespot.imperio.security.model.User;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "RECEIPT_DOCUMENT")
@Data
public class ReceiptDocument {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "DOCUMENT_ID")
    private Long documentId;

    @Column(name = "TOTAL_AMOUNT")
    private Double total;

    @ManyToOne
    @JoinColumn(name = "RECEIPT_HOLDER")
    private User holder;

    @ManyToOne
    @JoinColumn(name = "MODIFIED_BY")
    private User modifiedBy;

    @ManyToOne
    @JoinColumn(name = "CREATED_BY")
    private User createdBy;

    @Column(name = "DATE")
    private Date date;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_DATE")
    private Date modificationDate;

    @ManyToOne
    @JoinColumn(name = "VENDOR")
    private Vendor vendor;

    @ManyToOne
    @JoinColumn(name = "STATUS")
    private Status status;

}