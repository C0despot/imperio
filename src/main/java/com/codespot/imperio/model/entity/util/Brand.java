package com.codespot.imperio.model.entity.util;


import lombok.Data;

import javax.persistence.*;

/**
 * @author - Jose A. Rodriguez.
*/

@Data
@Entity
@Table(name = "BRAND")
public class Brand {

    @Column(name = "BRAND_ID")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "DESCRIPTION")
    private String name;
}