package com.codespot.imperio.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "CONDUCE")
public class Conduce {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Transient
    private List<ConduceProduct> products;

    @ManyToOne
    @JoinColumn(name = "INVOICE")
    private Invoice invoice;

    @Column(name = "CREATED")
    private Date created;
}
