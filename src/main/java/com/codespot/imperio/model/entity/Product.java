package com.codespot.imperio.model.entity;

import com.codespot.imperio.model.entity.util.*;
import com.codespot.imperio.model.entity.util.Package;
import com.codespot.imperio.security.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * @author - Jose A. Rodriguez.
 */

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "PRODUCT")
public class Product {

    @Column(name = "PRODUCT_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToOne
    @JoinColumn(name = "VENDOR")
    private Vendor vendor;

    @ManyToOne
    @JoinColumn(name = "BRAND")
    private Brand brand;

    @ManyToOne
    @JoinColumn(name = "PRICE_TYPE")
    private Price priceType;

    @ManyToOne
    @JoinColumn(name = "PACKAGE")
    private Package pack;

    @ManyToOne
    @JoinColumn(name = "CATEGORY")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "STATUS")
    private Status status;

    @ManyToOne
    @JoinColumn(name = "MODIFIED_BY")
    private User modifiedBy;

    @ManyToOne
    @JoinColumn(name = "CREATED_BY")
    private User createdBy;

    @Column(name = "GAIN_VALUE")
    private Double gain;

    @Column(name = "PURCHASE_PRICE")
    private Double purchasePrice;

    @Column(name = "SALE_PRICE")
    private Double salePrice;

    @Column(name = "WHOLESALE_PRICE")
    private Double wholesalePrice;

    @Column(name = "WHOLESALE_QTY")
    private Long wholesaleQuantity;

    @Column(name = "WHOLESALE_PCT")
    private Long wholesalePercent;

    @Column(name = "LINK_CODE")
    private Long linkCode;

    @Column(name = "TOTAL_AMOUNT")
    private Double totalAmount;

    @Column(name = "IMAGE")
    private String image;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "CODE")
    private String code;

    @Column(name = "POS_DESCRIPTION")
    private String posDescription;

    @Column(name = "ALLOW_TAX")
    private String allowTax;

    @Column(name = "LOCATION")
    private String location;


}
