package com.codespot.imperio.model.entity;

import com.codespot.imperio.security.model.User;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "OPEN_CASH_DRAW")
public class OpenCashBox {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "DOC_ID")
    private Long docId;

    @Column(name = "CREATED_DATE")
    private Date date;

    @Column(name = "TOTAL")
    private Integer totalAmount;

    @Lob
    @Column(name = "CURRENCY_OBJ")
    private String currencySelected;

    @ManyToOne
    @JoinColumn(name = "CREATED_BY")
    private User user;

    @Column(name = "TYPE")
    private String type;
}
