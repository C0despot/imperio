package com.codespot.imperio.model.entity.util;

import lombok.Data;

import javax.persistence.*;

/**
 * @author - Jose A. Rodriguez.
*/

@Data
@Entity
@Table(name = "PACKAGE")
public class Package {

    @Column(name = "PACKAGE_ID")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "DESCRIPTION")
    private String name;

    @Column(name = "UNITY_VALUE")
    private Double unityValue;
}