package com.codespot.imperio.model.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "CONDUCE_PRODUCT")
public class ConduceProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "DOCUMENT_ID")
    private Long docId;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "PRICE")
    private Double price;

    @Column(name = "QUANTITY")
    private Double qty;

    @Column(name = "PRODUCT_ID")
    private Long id;

    @Column(name = "CONDUCE")
    private Long conduce;

    @Column(name = "ITEM_CODE")
    private String code;

}
