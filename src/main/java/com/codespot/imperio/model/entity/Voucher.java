package com.codespot.imperio.model.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "VOUCHER")
public class Voucher {

    public Voucher() {
    }

    public static Voucher next(Voucher prev) {
        return new Voucher(prev);
    }

    private Voucher(Voucher previous) {
        this.type = previous.type;
        this.sequence = previous.sequence + 1;
        this.prefix = previous.prefix;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "VOUCHER_ID")
    private Long id;

    @Column(name = "SEQ_NUM")
    private Long sequence;

    @Column(name = "TYPE")
    private Short type;

    @Column(name = "PREFIX")
    private String prefix;

    public String getSequence() {
        return String.format("%08d", sequence);
    }
}
