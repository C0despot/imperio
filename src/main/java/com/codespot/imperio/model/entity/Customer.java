package com.codespot.imperio.model.entity;

import com.codespot.imperio.model.entity.util.Category;
import com.codespot.imperio.security.model.User;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CUSTOMER")
@Data
public class Customer {

    @Column(name = "CUST_ID")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "CUST_NAME")
    private String name;

    @Column(name = "CUST_ADDRESS")
    private String address;

    @Column(name = "IS_COMPANY")
    private String clientType;

    @Column(name = "ALLOW_CREDIT")
    private String allowCredit;

    @Column(name = "BALANCE")
    private Double balance;

    @Column(name = "PHONE_NO")
    private String phone;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @ManyToOne
    @JoinColumn(name = "MODIFIED_BY")
    private User modifiedBy;

    @ManyToOne
    @JoinColumn(name = "CREATED_BY")
    private User createdBy;

    @Column(name = "RNC")
    private String rnc;

    @Column(name = "ID_CARD_NUMBER")
    private String idCard;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "MAX_ALLOWED_CREDIT")
    private Double maxCredit;

    @Column(name = "BANK_ACCOUNT")
    private String bankAccount;
}
