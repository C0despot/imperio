package com.codespot.imperio.model.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "RECEIPT_DOCUMENT_PRODUCT")
@Data
public class ReceiptDocumentProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "DOCUMENT_ID")
    private Long docId;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "PRICE")
    private Double price;

    @Column(name = "SALE_PRICE")
    private Double salePrice;

    @Column(name = "QUANTITY")
    private Double qty;

    @Column(name = "PRODUCT_ID")
    private Long id;

    @Column(name = "RECEIPT_DOCUMENT")
    private Long document;

    @Column(name = "ITEM_CODE")
    private String code;

}
