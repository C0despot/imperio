package com.codespot.imperio.model.repository;

import com.codespot.imperio.model.entity.CashBox;
import com.codespot.imperio.model.entity.Customer;
import com.codespot.imperio.model.entity.Invoice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InvoiceRepository extends JpaRepository<Invoice, Long> {
    List<Invoice> findAllByCustomerEquals(Customer customer);
    Page<Invoice> findAllByCustomer_RncContaining(String customer_rnc, Pageable pageable);
    List<Invoice> findAllByCashBoxEquals(CashBox cashBox);
}
