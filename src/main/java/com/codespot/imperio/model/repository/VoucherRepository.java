package com.codespot.imperio.model.repository;

import com.codespot.imperio.model.entity.Voucher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface VoucherRepository extends JpaRepository<Voucher, Long> {

    @Query(value = "SELECT * FROM VOUCHER WHERE SEQ_NUM IN (SELECT MAX(SEQ_NUM)FROM VOUCHER WHERE TYPE = :seqType)", nativeQuery = true)
    Voucher findSequence(@Param(value = "seqType") Short seqType);
}
