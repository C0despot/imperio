package com.codespot.imperio.model.repository;

import com.codespot.imperio.model.entity.Conduce;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ConduceRepository extends JpaRepository<Conduce, Long> {
    List<Conduce> findAllByInvoiceInvoiceId(Long id);
}
