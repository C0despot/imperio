package com.codespot.imperio.model.repository.util;

import com.codespot.imperio.model.entity.util.Status;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusRepository extends JpaRepository<Status, Long> {
}
