package com.codespot.imperio.model.repository;

import com.codespot.imperio.model.entity.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long>{
    Page<Customer> findAllByNameContainingOrRncContainingOrPhoneContainingOrIdOrIdCardContaining(String name, String rnc, String phone, Long id, String idCard, Pageable pageable);
}
