package com.codespot.imperio.model.repository;

import com.codespot.imperio.model.entity.PosProduct;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PosProductRepository extends JpaRepository<PosProduct, Long> {
    List<PosProduct> findAllByInvoiceEquals(Long invoice);
}
