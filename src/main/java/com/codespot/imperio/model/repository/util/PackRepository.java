package com.codespot.imperio.model.repository.util;

import com.codespot.imperio.model.entity.util.Package;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PackRepository extends JpaRepository<Package, Long>{
}
