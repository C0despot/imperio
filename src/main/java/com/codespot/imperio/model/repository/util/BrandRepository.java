package com.codespot.imperio.model.repository.util;

import com.codespot.imperio.model.entity.util.Brand;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BrandRepository extends JpaRepository<Brand, Long> {
}
