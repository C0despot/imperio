package com.codespot.imperio.model.repository;

import com.codespot.imperio.model.entity.ReceiptDocumentProduct;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReceiptProductRepository extends JpaRepository<ReceiptDocumentProduct, Long> {
    Page<ReceiptDocumentProduct> findAllByCodeContainingOrDescriptionContainingOrIdOrDocument(String code, String desc, Long product, Long doc, Pageable pageable);
    List<ReceiptDocumentProduct> findAllByDocumentEquals(Long document);
}
