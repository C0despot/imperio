package com.codespot.imperio.model.repository.util;

import com.codespot.imperio.model.entity.util.Vendor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VendorRepository extends JpaRepository<Vendor, Long> {
}
