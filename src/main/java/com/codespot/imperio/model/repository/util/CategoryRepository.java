package com.codespot.imperio.model.repository.util;

import com.codespot.imperio.model.entity.util.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {
    Category findByNameEquals(String name);
}
