package com.codespot.imperio.model.repository;

import com.codespot.imperio.model.entity.OpenCashBox;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OpenCashRepository extends JpaRepository<OpenCashBox, Long> {
}
