package com.codespot.imperio.model.repository;

import com.codespot.imperio.model.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long>, JpaSpecificationExecutor<Product> {

    Page<Product> findAllByNameContainingOrDescriptionContainingOrCodeContainingOrPosDescriptionContaining(String name, String description, String code, String posDescription, Pageable pageable);
    Page<Product> findAllByCodeContaining(String code, Pageable pageable);

}
