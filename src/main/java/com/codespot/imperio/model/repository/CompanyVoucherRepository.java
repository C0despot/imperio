package com.codespot.imperio.model.repository;

import com.codespot.imperio.model.entity.CompanyVoucher;
import com.codespot.imperio.service.sales.SalesService;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyVoucherRepository extends JpaRepository<CompanyVoucher, SalesService.ReportOrigin> {
}
