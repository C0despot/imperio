package com.codespot.imperio.model.repository;

import com.codespot.imperio.model.entity.CashBox;
import com.codespot.imperio.model.entity.util.Status;
import com.codespot.imperio.model.enums.StatusType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;

public interface CashBoxRepository extends JpaRepository<CashBox, Long> {

    CashBox findByStatusEquals(StatusType.Sale status);

    Page<CashBox> findAllByCreatedDateGreaterThanEqualAndCreatedDateLessThanEqual(Date p1, Date p2, Pageable pageable);
}
