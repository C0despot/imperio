package com.codespot.imperio.model.repository;

import com.codespot.imperio.model.entity.Conduce;
import com.codespot.imperio.model.entity.ConduceProduct;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ConduceProductRepository extends JpaRepository<ConduceProduct, Long> {
    List<ConduceProduct> findAllByConduceEquals(Long conduce);
}
