package com.codespot.imperio.model.repository;

import com.codespot.imperio.model.entity.ReceiptDocument;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReceiptDocRepository extends JpaRepository<ReceiptDocument, Long> {
}
