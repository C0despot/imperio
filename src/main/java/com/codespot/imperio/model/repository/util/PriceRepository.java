package com.codespot.imperio.model.repository.util;

import com.codespot.imperio.model.entity.util.Price;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PriceRepository extends JpaRepository<Price, Long> {
}
