package com.codespot.imperio.model.enums;

public enum VoucherType {
    FINAL_CONSUMER(1001, "B02"),
    FISCAL_CREDIT(1010, "B01"),
    GOVERNMENTAL(1100, "B15"),
    SPECIAL(1110, "..."),
    CREDIT_NOTE(1101, "B04");

    VoucherType(int code, String prefix) {
        this.code = code;
        this.prefix = prefix;
    }

    public static String build(int code, String value) {
        for (VoucherType voucherCode : VoucherType.values()) {
            if (voucherCode.code != code && !voucherCode.equals(SPECIAL))
                return voucherCode.prefix + value;
        }
        return "...";
    }

    Integer code;
    String prefix;

    public static VoucherType get(Short code) {
        for (VoucherType type : VoucherType.values()) {
            if (type.code() == code)
                return type;
        }
        return SPECIAL;
    }

    public short code() {
        return code.shortValue();
    }
}
