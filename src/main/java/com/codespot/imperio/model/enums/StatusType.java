package com.codespot.imperio.model.enums;

public class StatusType {

    public enum Sale {
        PENDING,
        OPENED,
        ACTIVE,
        CANCELED,
        CLOSED
    }

    public enum Invoice {
        PENDING,
        DRAFTED,
        PROCESSED,
        CANCELED,
        CLOSED,
        PAYMENT_PENDING
    }

    public enum User {
        CREATED,
        VALIDATED,
        CANCELED,
        RESIGNED
    }
}
