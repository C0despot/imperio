package com.codespot.imperio.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExcelProduct {

    private String code;
    private String name;
    private Double cost;
    private Double price;
    private Double amount;
    private String category;

}
