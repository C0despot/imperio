package com.codespot.imperio.service.product;

import com.codespot.imperio.model.ExcelProduct;
import com.codespot.imperio.model.entity.Product;
import com.codespot.imperio.model.entity.util.Category;
import com.codespot.imperio.model.entity.util.Price;
import com.codespot.imperio.model.repository.ProductRepository;
import com.codespot.imperio.model.repository.util.CategoryRepository;
import com.codespot.imperio.security.model.User;
import com.codespot.imperio.security.repository.UserRepository;
import com.codespot.imperio.service.image.ImageService;
import com.codespot.imperio.specification.ProductSpecification;
import com.codespot.imperio.util.PageUtil;
import com.codespot.imperio.util.SearchUtil;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Log
@Service("defaultProductService")
public class DefaultProductService implements ProductService {

    private final
    ProductRepository productRepository;

    private final
    UserRepository userRepository;

    private final
    CategoryRepository categoryRepository;

    @Value(value = "http://codespotservice.000webhostapp.com/images/")
    private String baseImageServerUrl;

    private final
    ImageService imageService;

    @Autowired
    public DefaultProductService(ProductRepository productRepository, UserRepository userRepository, ImageService imageService, CategoryRepository categoryRepository) {
        this.productRepository = productRepository;
        this.userRepository = userRepository;
        this.imageService = imageService;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Page<Product> getProducts(PageUtil pageUtil) {
        Pageable pageable = new PageRequest(pageUtil.getPage(), pageUtil.getSize() == 0 ? 100 : pageUtil.getSize(), new Sort(Sort.Direction.DESC, "id"));
        return this.productRepository.findAll(pageable);
    }

    @Override
    @Transactional
    public ResponseEntity saveProduct(Product product, String username) {
        User user = this.userRepository.findUser(username);
        product.setModifiedBy(user);
        product.setModifiedDate(new Date());
        if (product.getId() == null) {
            product.setCreatedBy(user);
            product.setCreatedDate(new Date());
        }
        product = this.productRepository.save(product);
        if (product.getCode() == null || product.getCode().equalsIgnoreCase("----"))
            product.setCode((1000 + product.getId() + "").substring(6));
        product = this.productRepository.save(product);
        return ResponseEntity.ok(product);
    }

    @Override
    public Product getById(Long id) {
        return this.productRepository.findOne(id);
    }

    @Override
    @Transactional
    public String changeProductImage(MultipartFile file, Long productId) throws IOException {
        return this.imageService.upload(file, productId);
    }

    @Override
    public ResponseEntity search(PageUtil pageUtil, SearchUtil searchUtil) {
        Pageable pageable = new PageRequest(pageUtil.getPage(), pageUtil.getSize(), new Sort(Sort.Direction.fromString(searchUtil.getOrder()), searchUtil.getSearchBy()));

        ProductSpecification specification = new ProductSpecification(searchUtil);
        Page<Product> products = this.productRepository.findAll(Specifications.where(specification), pageable);

        if (products.getContent().size() == 0)
            return ResponseEntity.unprocessableEntity().build();
        return ResponseEntity.ok(products);
    }

    @Override
    public ResponseEntity filter(String term) {
        Pageable pageable = new PageRequest(0, 50, new Sort(Sort.Direction.DESC, "id"));
        return ResponseEntity.ok(this.productRepository.findAllByNameContainingOrDescriptionContainingOrCodeContainingOrPosDescriptionContaining(term, term, term, term, pageable).getContent());
    }

    @Override
    @Transactional
    public void validateProductTransaction(Long productId, Double amount, Boolean isProcessed) {
        if (isProcessed) {
            Product product = this.productRepository.findOne(productId);
            product.setTotalAmount(amount + product.getTotalAmount());
            if (product.getTotalAmount() < 0)
                throw new RuntimeException("La Cantidad de Productos no debe ser igual a 0.");
            this.productRepository.save(product);

        }
    }

    @Override
    public void massUpload(List<ExcelProduct> products) {

        Price price = Price.builder()
                .id(1000000003L).build();
        products.forEach(excelProduct -> {
            Product product = Product
                    .builder()
                    .code(excelProduct.getCode())
                    .name(excelProduct.getName())
                    .salePrice(excelProduct.getPrice())
                    .purchasePrice(excelProduct.getCost())
                    .totalAmount(excelProduct.getAmount())
                    .category(getCategory(excelProduct.getCategory()))
                    .createdDate(new Date())
                    .modifiedDate(new Date())
                    .priceType(price)
                    .description("GENERATED FROM EXCEL FILE AND MUST BE COMPLETED")
                    .posDescription(excelProduct.getName())
                    .build();
            this.productRepository.save(product);
        });
    }

    private Category getCategory(String description) {
        Category category = this.categoryRepository.findByNameEquals(description);
        if (Objects.isNull(category)) {
            category = Category.builder().name(description.toUpperCase()).build();
            category = this.categoryRepository.save(category);
        }
        return category;
    }
}
