package com.codespot.imperio.service.product;

import com.codespot.imperio.model.ExcelProduct;
import com.codespot.imperio.model.entity.Product;
import com.codespot.imperio.util.PageUtil;
import com.codespot.imperio.util.SearchUtil;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface ProductService {

    Page<Product> getProducts(PageUtil pageUtil);
    ResponseEntity saveProduct(Product product, String username);
    Product getById(Long id);
    String changeProductImage(MultipartFile file, Long productId) throws IOException;
    ResponseEntity search(PageUtil pageUtil, SearchUtil searchUtil);
    ResponseEntity filter(String term);
    void validateProductTransaction(Long productId, Double amount, Boolean isProcessed);
    void massUpload(List<ExcelProduct> products);
}
