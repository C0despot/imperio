package com.codespot.imperio.service.sales;

import com.codespot.imperio.model.entity.*;
import com.codespot.imperio.model.entity.util.Status;
import com.codespot.imperio.model.enums.StatusType;
import com.codespot.imperio.model.enums.VoucherType;
import com.codespot.imperio.model.repository.*;
import com.codespot.imperio.security.model.User;
import com.codespot.imperio.service.product.ProductService;
import com.codespot.imperio.service.utility.UtilityService;
import com.codespot.imperio.util.PageUtil;
import com.codespot.imperio.util.SearchUtil;
import com.codespot.imperio.util.SqrtUtil;
import com.codespot.imperio.web.dto.InvoiceDto;
import com.rodal.printer.*;
import com.rodal.printer.pojo.*;
import lombok.extern.java.Log;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Log
@Service("defaultSalesService")
public class DefaultSalesService implements SalesService {


    private final
    DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

    private final
    ModelMapper modelMapper;

    private final
    UtilityService utilityService;

    private final
    InvoiceRepository invoiceRepository;

    private final
    PosProductRepository soldProductRepository;

    private final
    ProductRepository productRepository;

    private final
    CustomerRepository customerRepository;

    private final
    ProductService productService;

    private final
    CashBoxRepository cashBoxRepository;

    private final
    OpenCashRepository openCashRepository;

    private final
    CompanyVoucherRepository voucherRepository;

    private final
    ConduceRepository conduceRepository;

    private final
    ConduceProductRepository conduceProductRepository;

    @Autowired
    public DefaultSalesService(ModelMapper modelMapper, UtilityService utilityService, InvoiceRepository invoiceRepository, PosProductRepository soldProductRepository, ProductRepository productRepository, CustomerRepository customerRepository, ProductService productService, CashBoxRepository cashBoxRepository, OpenCashRepository openCashRepository, CompanyVoucherRepository voucherRepository, ConduceRepository conduceRepository, ConduceProductRepository conduceProductRepository) {
        this.modelMapper = modelMapper;
        this.utilityService = utilityService;
        this.invoiceRepository = invoiceRepository;
        this.soldProductRepository = soldProductRepository;
        this.productRepository = productRepository;
        this.customerRepository = customerRepository;
        this.productService = productService;
        this.cashBoxRepository = cashBoxRepository;
        this.openCashRepository = openCashRepository;
        this.voucherRepository = voucherRepository;
        this.conduceRepository = conduceRepository;
        this.conduceProductRepository = conduceProductRepository;
    }

    @Override
    @Transactional
    public ResponseEntity process(InvoiceDto invoiceDto) {
        Invoice invoice = modelMapper.map(invoiceDto, Invoice.class);
        invoice.setModifiedBy(this.utilityService.loggedUser());
        if (invoiceDto.getInvoiceId() == null)
            invoice.setCreatedBy(this.utilityService.loggedUser());
        invoice = this.invoiceRepository.save(invoice);
        List<PosProduct> products = Arrays.asList(modelMapper.map(invoiceDto.getProducts(), PosProduct[].class));
        Invoice finalInvoice = invoice;
        this.validateItemList(products);
        final Boolean isProcessed = invoiceDto.getIsProcessed();
        products.forEach((product) -> {
            product.setInvoice(finalInvoice.getInvoiceId());
            this.productService.validateProductTransaction(product.getId(), -product.getQty(), isProcessed);
        });

        //TODO process of add voucher
        products = this.soldProductRepository.save(products);
        invoiceDto = modelMapper.map(invoice, InvoiceDto.class);
        invoiceDto.setProducts(products);
        if (isProcessed) {
            invoice.setVoucherNo(generateVoucher(invoice));
        }
        return ResponseEntity.ok(invoiceDto);
    }

    private String generateVoucher(Invoice invoice) {
        CompanyVoucher prev = this.voucherRepository.getOne(invoice.getCompany());
        String next = prev.next(invoice.getVoucherType());
        this.voucherRepository.save(prev);
        return VoucherType.build(invoice.getVoucherType(), next);
    }

    @Override
    public ResponseEntity getSales(PageUtil pageUtil, String searchBy, String criteria) {
        List<Invoice> invoices = new ArrayList<>();
        if (criteria.length() > 0)
            switch (searchBy) {
                case "client":
                    Pageable pageable = new PageRequest(pageUtil.getPage(), pageUtil.getSize(), new Sort(Sort.Direction.DESC, "id"));
                    Long id = 0L;
                    try {
                        id = new Long(criteria);
                    } catch (Exception ignored) {
                    }
                    List<Customer> customers = this.customerRepository.findAllByNameContainingOrRncContainingOrPhoneContainingOrIdOrIdCardContaining(criteria, criteria, criteria, id, criteria, pageable).getContent();
                    List<Invoice> finalInvoices = invoices;
                    customers.forEach((customer -> {
                        finalInvoices.addAll(this.invoiceRepository.findAllByCustomerEquals(customer));
                    }));
                    invoices = finalInvoices;
                    break;
                case "rnc":
                    pageable = new PageRequest(pageUtil.getPage(), pageUtil.getSize(), new Sort(Sort.Direction.DESC, "date"));
                    invoices = this.invoiceRepository.findAllByCustomer_RncContaining(criteria, pageable).getContent();
                    break;
                case "invoiceId":
                    try {
                        Invoice invoice = this.invoiceRepository.findOne(Long.parseLong(criteria));
                        invoices.add(invoice);
                    } catch (Exception ignored) {
                    }
                    break;
            }
        else {
            Pageable pageable = new PageRequest(pageUtil.getPage(), pageUtil.getSize(), new Sort(Sort.Direction.DESC, "date"));
            invoices.addAll(this.invoiceRepository.findAll(pageable).getContent());
        }
        if (invoices.size() == 0) throw new RuntimeException("Nada Encontrado.");
        return ResponseEntity.ok(invoices);
    }

    @Override
    public ResponseEntity getOne(Long id) {
        Invoice invoice = this.invoiceRepository.findOne(id);
        if (invoice == null)
            return ResponseEntity.notFound().build();
        List<PosProduct> posProducts = this.soldProductRepository.findAllByInvoiceEquals(invoice.getInvoiceId());
        if (posProducts == null)
            return ResponseEntity.notFound().build();
        InvoiceDto invoiceDto = this.modelMapper.map(invoice, InvoiceDto.class);
        invoiceDto.setProducts(posProducts);
        return ResponseEntity.ok(invoiceDto);
    }

    @Override
    @Transactional
    public Customer saveCustomer(Customer customer) {
        User loggedUser = this.utilityService.loggedUser();
        if (customer.getId() == null) {
            customer.setCreatedBy(loggedUser);
            customer.setCreatedDate(new Date());
        }
        customer.setModifiedDate(new Date());
        customer.setModifiedBy(loggedUser);
        customer = this.customerRepository.save(customer);
        return customer;
    }

    @Override
    @Transactional
    public ResponseEntity processDrafted(Long id) {
        Invoice invoice = this.invoiceRepository.findOne(id);
        invoice.setStatus(StatusType.Invoice.PROCESSED);
        List<PosProduct> posProducts = this.soldProductRepository.findAllByInvoiceEquals(invoice.getInvoiceId());
        this.invoiceRepository.save(invoice);
        posProducts.forEach((product) -> this.productService.validateProductTransaction(product.getId(), -product.getQty(), true));
        //TODO process of add voucher
        invoice.setVoucherNo(generateVoucher(invoice));
        return ResponseEntity.ok(invoice);
    }

    @Override
    public ResponseEntity getCustomers(PageUtil pageUtil, SearchUtil searchUtil) {
        Pageable pageable = new PageRequest(pageUtil.getPage() - 1, pageUtil.getSize(), new Sort(Sort.Direction.DESC, "id"));
        Long id = 0L;
        String criteria = searchUtil.getCriteria();
        try {
            id = new Long(criteria);
        } catch (Exception ignored) {
        }
        return ResponseEntity.ok(this.customerRepository
                .findAllByNameContainingOrRncContainingOrPhoneContainingOrIdOrIdCardContaining(criteria, criteria, criteria, id, criteria, pageable));
    }

    @Override
    public ResponseEntity getSqrtNextStep(Principal principal) {
        SqrtUtil util = new SqrtUtil(-1);
        if (principal == null)
            return ResponseEntity.ok(util);
        CashBox cashBox = this.cashBoxRepository.findByStatusEquals(StatusType.Sale.OPENED);
        if (cashBox == null)
            return ResponseEntity.notFound().build();
        int days = getDays(cashBox.getCreatedDate());

        util = new SqrtUtil(days);
        return ResponseEntity.ok(util);
    }

    @Override
    @Transactional
    public ResponseEntity openDay(OpenCashBox openCashBox) {

        User user = this.utilityService.loggedUser();
        openCashBox.setDate(new Date());
        openCashBox.setUser(user);

        if (openCashBox.getType().equalsIgnoreCase("O")) {
            openCashBox = this.openCashRepository.save(openCashBox);
            CashBox cashBox = new CashBox();

            cashBox.setCreatedDate(new Date());
            cashBox.setCreatedBy(user);
            cashBox.setStatus(StatusType.Sale.OPENED);
            cashBox.setStartCash(openCashBox.getTotalAmount().doubleValue());
            cashBox.setOpenCashBox(openCashBox);
            this.cashBoxRepository.save(cashBox);
        }

        return ResponseEntity.ok(openCashBox);
    }

    @Override
    public ResponseEntity getCurrentlySqrt() {

        CashBox cashBox = this.cashBoxRepository.findByStatusEquals(StatusType.Sale.OPENED);

        if (cashBox == null)
            return ResponseEntity.notFound().build();


        List<Invoice> pendingSales = getPendingSales();

        double totalCash = 0;
        double totalCreditCard = 0;
        double totalCheck = 0;
        double totalCredit = 0;
        double totalTax = 0;

        for (Invoice sale : pendingSales) {
            totalTax += sale.getTax();

            switch (sale.getPayment()) {
                case 1001:
                    totalCash += Double.parseDouble(sale.getTotal().replace(",", ""));
                    break;
                case 1010:
                    totalCreditCard += Double.parseDouble(sale.getTotal().replace(",", ""));
                    break;
                case 1100:
                    totalCheck += Double.parseDouble(sale.getTotal().replace(",", ""));
                    break;
                case 1101:
                    totalCredit += Double.parseDouble(sale.getTotal().replace(",", ""));
                    break;
            }
        }

        cashBox.setTotal(getCurrencyValue(totalCash + totalCredit + totalCreditCard + totalCheck));
        cashBox.setTotalCash(getCurrencyValue(totalCash));
        cashBox.setTotalCheck(getCurrencyValue(totalCheck));
        cashBox.setTotalCreditCard(getCurrencyValue(totalCreditCard));
        cashBox.setTotalCredit(getCurrencyValue(totalCredit));
        cashBox.setTax(getCurrencyValue(totalTax));
        return ResponseEntity.ok(cashBox);
    }

    @Override
    @Transactional
    public ResponseEntity terminateCashBox(CashBox cashBox) {
        Status status = this.utilityService.getStatus(1000000002L);
        cashBox.setStatus(StatusType.Sale.CLOSED);
        OpenCashBox close = cashBox.getCloseCashBox();
        close = this.openCashRepository.save(close);
        cashBox.setCloseCashBox(close);
        cashBox = this.cashBoxRepository.save(cashBox);
        final CashBox cb = cashBox;
        List<Invoice> invoices = getPendingSales();

        invoices.forEach((invoice -> {
            invoice.setCashBox(cb);
        }));

        this.invoiceRepository.save(invoices);

        return ResponseEntity.ok(cashBox);
    }

    @Override
    public ResponseEntity getBoxes(PageUtil pageUtil, SearchUtil searchUtil) throws ParseException {
        Pageable pageable = new PageRequest(pageUtil.getPage(), pageUtil.getSize(), new Sort(Sort.Direction.DESC, "createdDate"));

        Date startDate = this.formatter.parse(searchUtil.getStartDate());
        Date endDate = this.formatter.parse(searchUtil.getEndDate());
        endDate = addDays(endDate, 1);
        Page<CashBox> cashBoxes = this.cashBoxRepository.findAllByCreatedDateGreaterThanEqualAndCreatedDateLessThanEqual(startDate, endDate, pageable);
        return ResponseEntity.ok(cashBoxes);
    }

    @Override
    public void printReport(ReportType type, String path, Long doc) {
        AbstractInvoicePrinter invoiceBuilder = null;
        PrintCompanyInfo info = new PrintCompanyInfo();
        Invoice invoice = this.invoiceRepository.findOne(doc);
        ReportOrigin origin = invoice.getCompany();
        PrintQuoteDetail quoteDetail = null;
        if (type.equals(ReportType.INVOICE)) {
            if (origin.equals(ReportOrigin.RVIMP)) {
                invoiceBuilder = RvInvoiceBuilder.of(path, "DOP");
                info = PrintCompanyInfo
                        .builder()
                        .name("RV Imperio Electrico")
                        .rnc("130-52826-8")
                        .rpe("1245")
                        .type("FACTURA")
                        .build();
            } else if (origin.equals(ReportOrigin.RANSA)) {
                invoiceBuilder = RansaInvoiceBuilder.of(path, "DOP");
                info = PrintCompanyInfo
                        .builder()
                        .name("Ransa, SRL.")
                        .rnc("131-55995-6")
                        .rpe("1245")
                        .type("FACTURA")
                        .build();
            } else if (origin.equals(ReportOrigin.MRAMI)) {
                invoiceBuilder = MercantilInvoiceBuilder.of(path, "DOP");
                info = PrintCompanyInfo
                        .builder()
                        .name("Mercantil Rami, SRL.")
                        .rnc("122-02475-1")
                        .rpe("1245")
                        .type("FACTURA")
                        .build();
            }
        } else if (type.equals(ReportType.QUOTE)) {
            quoteDetail = PrintQuoteDetail.builder()
                    .attention("Dpto. de Compras")
                    .email("- - -")
                    .shiftTime(invoice.getShiftTime())
                    .validity(invoice.getValidity())
                    .procedure(invoice.getProcedureRef())
                    .condition(invoice.getPaymentCondition())
                    .build();
            if (origin.equals(ReportOrigin.RVIMP)) {
                invoiceBuilder = RVQuotationBuilder.of(path, "DOP");
                info = PrintCompanyInfo
                        .builder()
                        .name("RV Imperio Electrico")
                        .rnc("130-52826-8")
                        .rpe("1245")
                        .type("FACTURA")
                        .build();
            } else if (origin.equals(ReportOrigin.MRAMI)) {
                invoiceBuilder = MercantilQuotationBuilder.of(path, "DOP");
                info = PrintCompanyInfo
                        .builder()
                        .name("Mercantil Rami, SRL.")
                        .rnc("122-02475-1")
                        .rpe("1245")
                        .type("FACTURA")
                        .build();
            }
        }
        List<PosProduct> posProducts = this.soldProductRepository.findAllByInvoiceEquals(invoice.getInvoiceId());
        Customer customer = invoice.getCustomer();
        PrintCustomer printCustomer = PrintCustomer
                .builder()
                .city(customer.getAddress())
                .countryId("DR")
                .name(customer.getName())
                .rnc(customer.getClientType().equals("0") ? customer.getRnc() : customer.getIdCard())
                .id(customer.getId().intValue())
                .phone(customer.getPhone())
                .street("")
                .build();
        List<PrintItem> items = new ArrayList<>();
        posProducts.forEach(posProduct -> {
            Product product = this.productRepository.findOne(posProduct.getId());
            PrintProduct printProduct = PrintProduct
                    .builder()
                    .id(posProduct.getId().intValue())
                    .name(posProduct.getDescription())
                    .price(posProduct.getPrice())
                    .vat((product.getAllowTax() != null && product.getAllowTax().startsWith("Y")) ? 18 : 0)
                    .build();
            PrintItem item = PrintItem
                    .builder()
                    .quantity(posProduct.getQty().intValue())
                    .product(printProduct)
                    .build();
            items.add(item);
        });
        PrintInvoice printInvoice = PrintInvoice
                .builder()
                .quoteDetail(quoteDetail)
                .customer(printCustomer)
                .orderNo(invoice.getOrderNo())
                .ncf(invoice.getVoucherNo())
                .id(invoice.getInvoiceId().intValue())
                .invoiceDate(invoice.getDate())
                .items(items)
                .build();
        if (type.isQuote())
            invoiceBuilder.buildQuote(printInvoice, info);
        else
            invoiceBuilder.createPdf(printInvoice, info);
    }

    @Override
    public ResponseEntity getVoucherInfo(ReportOrigin origin) {
        CompanyVoucher one = this.voucherRepository.findOne(origin);
        if (one == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(one);
    }

    @Override
    public ResponseEntity saveVoucherInfo(CompanyVoucher voucher) {
        voucher = this.voucherRepository.save(voucher);
        return ResponseEntity.ok(voucher);
    }

    @Override
    public ResponseEntity saveConduce(Conduce conduce) {
        List<ConduceProduct> products = conduce.getProducts();
        conduce = this.conduceRepository.save(conduce);
        products.forEach((product) -> product.setConduce(product.getConduce()));
        products = this.conduceProductRepository.save(products);
        conduce.setProducts(products);
        return ResponseEntity.ok(conduce);
    }

    @Override
    public ResponseEntity getConduces(Long invoice) {
        List<Conduce> conduces = this.conduceRepository.findAllByInvoiceInvoiceId(invoice);
        conduces.forEach((conduce -> {
            List<ConduceProduct> products = this.conduceProductRepository.findAllByConduceEquals(conduce.getId());
            conduce.setProducts(products);
        }));
        return ResponseEntity.ok(conduces);
    }

    private void validateItemList(List<PosProduct> products) {
        products.forEach(product -> {
            Product item = this.productRepository.findOne(product.getId());
            if (item.getTotalAmount() < product.getQty()) {
                throw new RuntimeException("Ha excedido la cantidad disponible" +
                        " de productos. " + product.getDescription() + " - [" + item.getTotalAmount() + "]");
            }
        });
    }

    private List<Invoice> getPendingSales() {
        return this.invoiceRepository.findAllByCashBoxEquals(null);
    }

    private String getCurrencyValue(Double amount) {
        return String.format("%,.2f", amount);
    }

    private Date addDays(Date date, int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, days);
        return calendar.getTime();
    }

    private int getDays(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int dayOne = cal.get(Calendar.DAY_OF_YEAR);
        cal.setTime(new Date());
        int dayTwo = cal.get(Calendar.DAY_OF_YEAR);
        return dayTwo - dayOne;
    }

}
