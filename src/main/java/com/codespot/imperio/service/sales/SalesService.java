package com.codespot.imperio.service.sales;

import com.codespot.imperio.model.entity.*;
import com.codespot.imperio.util.PageUtil;
import com.codespot.imperio.util.SearchUtil;
import com.codespot.imperio.web.dto.InvoiceDto;
import org.springframework.http.ResponseEntity;

import java.security.Principal;
import java.text.ParseException;

public interface SalesService {

    ResponseEntity process(InvoiceDto invoiceDto);

    ResponseEntity getSales(PageUtil pageUtil, String searchBy, String criteria);

    ResponseEntity getOne(Long id);

    Customer saveCustomer(Customer customer);

    ResponseEntity processDrafted(Long id);

    ResponseEntity getCustomers(PageUtil pageUtil, SearchUtil searchUtil);

    ResponseEntity getSqrtNextStep(Principal principal);

    ResponseEntity openDay(OpenCashBox openCashBox);

    ResponseEntity getCurrentlySqrt();

    ResponseEntity terminateCashBox(CashBox cashBox);

    ResponseEntity getBoxes(PageUtil pageUtil, SearchUtil searchUtil) throws ParseException;

    void printReport(ReportType type, String path, Long doc);

    ResponseEntity getVoucherInfo(ReportOrigin origin);

    ResponseEntity saveVoucherInfo(CompanyVoucher voucher);

    ResponseEntity saveConduce(Conduce conduce);

    ResponseEntity getConduces(Long invoice);

    enum ReportOrigin {
        RANSA,
        RVIMP,
        MRAMI,
    }

    enum ReportType {
        INVOICE,
        QUOTE,
        ;

        public boolean isQuote() {
            return this == QUOTE;
        }
    }

    enum VoucherCode {
        FINAL_CONSUMER(1001, "B02"),
        FISCAL_CREDIT(1010, "B01"),
        GOVERNMENTAL(1100, "B15"),
        CREDIT_NOTE(1101, "B04");

        VoucherCode(int code, String prefix) {
            this.code = code;
            this.prefix = prefix;
        }

        static String build(int code, String value) {
            for (VoucherCode voucherCode : VoucherCode.values()) {
                if (voucherCode.code == code)
                    return voucherCode.prefix + value;
            }
            return "...";
        }

        int code;
        String prefix;
    }
}
