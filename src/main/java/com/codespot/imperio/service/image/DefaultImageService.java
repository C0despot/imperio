package com.codespot.imperio.service.image;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.GroupGrantee;
import com.amazonaws.services.s3.model.Permission;
import com.amazonaws.services.s3.model.PutObjectRequest;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Log
@Service(value = "defaultImageService")
public class DefaultImageService implements ImageService {


    private String BUCKET_NAME;
    private String PRODUCT_IMG_EXT;
    private String AWS_ACCESS_KEY;
    private String AWS_SECRET_KEY;
    private String S3_BASE_URL;


    @Autowired
    public DefaultImageService(@Value(value = "${aws.s3.main.bucket}") String mainBucket,
                               @Value(value = "${aws.s3.main.product}") String productImgExtension,
                               @Value(value = "${aws.accessKeyId}") String accessKey,
                               @Value(value = "${aws.secretKey}") String secretKey,
                               @Value(value = "${aws.s3.base.url}") String s3BaseUrl) {
        this.BUCKET_NAME = mainBucket;
        this.PRODUCT_IMG_EXT = productImgExtension;
        this.AWS_ACCESS_KEY = accessKey;
        this.AWS_SECRET_KEY = secretKey;
        this.S3_BASE_URL = s3BaseUrl;
    }

    @Override
    public String upload(MultipartFile file, Long productId) throws IOException {

        String fileName = productId.toString() + "." + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf('.') + 1);
        String bucketDir = BUCKET_NAME + PRODUCT_IMG_EXT;
        BasicAWSCredentials awsCredentials = new BasicAWSCredentials(AWS_ACCESS_KEY, AWS_SECRET_KEY);
        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withRegion(Regions.US_EAST_1)
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .build();

        AccessControlList acl = new AccessControlList();
        acl.grantPermission(GroupGrantee.AllUsers, Permission.Read);
        File myFile = multipartToFile(file);
        s3Client
                .putObject(new PutObjectRequest(bucketDir, fileName, myFile)
                        .withAccessControlList(acl));
        return S3_BASE_URL + bucketDir + "/" + fileName;
    }

    private File multipartToFile(MultipartFile multipart) throws IOException {
        File newFile = new File(System.getProperty("java.io.tmpdir") +
                System.getProperty("file.separator") +
                multipart.getOriginalFilename());
        multipart.transferTo(newFile);
        return newFile;
    }

    @Override
    public String getCover(Long productId) {
        return null;
    }
}