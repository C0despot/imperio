package com.codespot.imperio.service.image;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface ImageService {

    String upload(MultipartFile file, Long productId) throws IOException;
    String getCover(Long productId);
}
