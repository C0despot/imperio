package com.codespot.imperio.service.audit;

import com.codespot.imperio.util.PageUtil;
import com.codespot.imperio.util.SearchUtil;
import com.codespot.imperio.web.dto.ReceiptDocumentDto;
import org.springframework.http.ResponseEntity;

public interface AuditService {

    ResponseEntity processReceiptDocument(ReceiptDocumentDto documentDto, String username);

    ResponseEntity getReceiptDocuments(PageUtil pageUtil, SearchUtil searchUtil);

    ResponseEntity getExtendedReceiptDocument(Long docId);
}
