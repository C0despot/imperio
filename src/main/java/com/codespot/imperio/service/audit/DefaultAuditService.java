package com.codespot.imperio.service.audit;

import com.codespot.imperio.model.entity.Product;
import com.codespot.imperio.model.entity.ReceiptDocument;
import com.codespot.imperio.model.entity.ReceiptDocumentProduct;
import com.codespot.imperio.model.entity.util.Status;
import com.codespot.imperio.model.repository.ProductRepository;
import com.codespot.imperio.model.repository.ReceiptDocRepository;
import com.codespot.imperio.model.repository.ReceiptProductRepository;
import com.codespot.imperio.security.model.User;
import com.codespot.imperio.security.repository.UserRepository;
import com.codespot.imperio.service.utility.UtilityService;
import com.codespot.imperio.util.PageUtil;
import com.codespot.imperio.util.SearchUtil;
import com.codespot.imperio.web.dto.ReceiptDocumentDto;
import lombok.extern.java.Log;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service(value = "defaultAuditService ")
@Log
public class DefaultAuditService implements AuditService {

    private final
    UserRepository userRepository;

    private final
    ReceiptDocRepository receiptDocRepository;

    private final
    ModelMapper modelMapper;

    private final
    UtilityService utilityService;

    private final
    ProductRepository productRepository;

    private final
    ReceiptProductRepository receiptProductRepository;

    @Autowired
    public DefaultAuditService(UserRepository userRepository, ReceiptDocRepository receiptDocRepository, ModelMapper modelMapper, UtilityService utilityService, ProductRepository productRepository, ReceiptProductRepository receiptProductRepository) {
        this.userRepository = userRepository;
        this.receiptDocRepository = receiptDocRepository;
        this.modelMapper = modelMapper;
        this.utilityService = utilityService;
        this.productRepository = productRepository;
        this.receiptProductRepository = receiptProductRepository;
    }

    @Override
    @Transactional
    public ResponseEntity processReceiptDocument(ReceiptDocumentDto documentDto, String username) {
        User user = this.userRepository.findUser(username);
        if (user == null)
            return ResponseEntity.status(401).body("Usuario no logeado");
        Status status;
        if (documentDto.getIsFinished())
            status = this.utilityService.getStatus(1000000002L);
        else status = this.utilityService.getStatus(1000000007L);

        ReceiptDocument document = this.modelMapper.map(documentDto, ReceiptDocument.class);
        document.setStatus(status);
        if (document.getDocumentId() == null) {
            document.setCreatedBy(user);
            document.setCreatedDate(new Date());
        }
        document.setModifiedBy(user);
        document.setModificationDate(new Date());

        documentDto.getProducts().remove(documentDto.getProducts().size() - 1);
        document = this.receiptDocRepository.save(document);
        final long doc = document.getDocumentId();
        List<ReceiptDocumentProduct> products = documentDto.getProducts();

        products.forEach((p) -> {

            if (p.getDocId() == null) {
                Product product = this.productRepository.findOne(p.getId());
                product.setPurchasePrice(p.getPrice());
                product.setSalePrice(p.getSalePrice());
                product.setTotalAmount(product.getTotalAmount() + p.getQty());
                this.productRepository.save(product);
            }
            p.setDocument(doc);
        });
        products = this.receiptProductRepository.save(products);

        documentDto.setDocumentId(doc);
        documentDto.setProducts(products);
        documentDto.setModifiedBy(document.getModifiedBy());
        documentDto.setCreatedBy(document.getCreatedBy());
        documentDto.setModificationDate(document.getModificationDate());
        documentDto.setCreatedDate(document.getCreatedDate());

        return ResponseEntity.ok(document);
    }

    @Override
    public ResponseEntity getReceiptDocuments(PageUtil pageUtil, SearchUtil searchUtil) {

        switch (searchUtil.getSearchBy()) {
            case "product": {
                Pageable pageable = new PageRequest(pageUtil.getPage(), pageUtil.getSize(), new Sort(Sort.Direction.fromString(searchUtil.getOrder()), "modificationDate"));
//                String search = searchUtil.getCriteria();
//                Long id = 0L;
//                try {
//                    id = new Long(search);
//                } catch (Exception ignored) {
//                }
//                Page<ReceiptDocumentProduct> products =
//                        this.receiptProductRepository
//                                .findAllByCodeContainingOrDescriptionContainingOrIdOrDocument(search, search, id, id, pageable);
                Page<ReceiptDocument> documents = this.receiptDocRepository.findAll(pageable);
                return ResponseEntity.ok(documents);
            }
            default:
                return ResponseEntity.status(404).body("Anything found");
        }
    }

    @Override
    public ResponseEntity getExtendedReceiptDocument(Long docId) {
        ReceiptDocument document = this.receiptDocRepository.findOne(docId);
        ReceiptDocumentDto documentDto = this.modelMapper.map(document, ReceiptDocumentDto.class);
        List<ReceiptDocumentProduct> products = this.receiptProductRepository.findAllByDocumentEquals(docId);
        documentDto.setProducts(products);
        return ResponseEntity.ok(documentDto);
    }
}
