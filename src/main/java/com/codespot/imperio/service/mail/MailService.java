package com.codespot.imperio.service.mail;

import com.codespot.imperio.security.model.User;

import javax.mail.MessagingException;
import java.security.GeneralSecurityException;

public interface MailService {

    void sendWelcomeEmail(User user) throws MessagingException, GeneralSecurityException;
}
