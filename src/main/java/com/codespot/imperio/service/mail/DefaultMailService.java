package com.codespot.imperio.service.mail;

import com.codespot.imperio.security.model.User;
import com.sun.mail.util.MailSSLSocketFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import sun.security.ssl.SSLSocketFactoryImpl;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.net.ssl.SSLSocketFactory;
import java.security.GeneralSecurityException;
import java.util.Properties;

@Service
public class DefaultMailService implements MailService {


    @Value(value = "smtp.gmail.com")
    private String host;

    @Value(value = "joseanibalrl76@gmail.com")
    private String email;

    @Value(value = "emelaniolopez")
    private String password;

    @Override
    public void sendWelcomeEmail(User user) throws MessagingException, GeneralSecurityException {
        Properties props = new Properties();
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", this.host);
        props.put("mail.smtp.port", "587");

        InternetAddress[] addresses = new InternetAddress[1];
        addresses[0] = new InternetAddress(user.getEmail());

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(email, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(this.email));
            message.setRecipients(Message.RecipientType.TO,
                    addresses);
            message.setSubject("BIENVENIDO AL EQUIPO..!");

            String url = "http://portal.imperioelectrico.com/#/" + user.getPassword() + "/welcome";
            String welcome = "Hola, " +
                    user.getFirstName() + " " + user.getLastName() + " . Bienvenido a Imperio Electrico." +
                    "<br> A continuacion podras abrir la URL en la cual se te pedira una contraseña y alguna" +
                    " informacion basica que puede quedar pendiente para el proceso de registracion. <br><br>";
            String html = "\n<label> " + welcome + "\nClick " +
                    "<a href='" + url + "'>aqui</a> " +
                    "para continuar o copie la siguiente direccion en su navegador: \n" +
                    " <br><a href='" + url + "'>" + url + "</a>" +
                    "</label> ";
            message.setText(html);
            message.setContent(html, "text/html; charset=utf-8");
            Transport.send(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}

