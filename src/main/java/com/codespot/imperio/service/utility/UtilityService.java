package com.codespot.imperio.service.utility;

import com.codespot.imperio.model.entity.util.*;
import com.codespot.imperio.model.entity.util.Package;
import com.codespot.imperio.security.model.User;
import com.codespot.imperio.util.DependencyUtil;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UtilityService {

    List<Category> getCategoryList();

    List<Price> getPriceList();

    List<Status> getStatusList();

    List<Brand> getBrandList();

    List<Package> getPackageList();

    List<Vendor> getVendorList();

    ResponseEntity saveDependency(DependencyUtil util);

    ResponseEntity getClients(String term);

    ResponseEntity getEmployees(String term);

    Status getStatus(Long id);
    User loggedUser();
}
