package com.codespot.imperio.service.utility;

import com.codespot.imperio.model.entity.Customer;
import com.codespot.imperio.model.entity.util.*;
import com.codespot.imperio.model.entity.util.Package;
import com.codespot.imperio.model.repository.CustomerRepository;
import com.codespot.imperio.model.repository.util.*;
import com.codespot.imperio.security.model.User;
import com.codespot.imperio.security.repository.UserRepository;
import com.codespot.imperio.util.DependencyUtil;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Log
@Service("defaultUtilityService")
public class DefaultUtilityService implements UtilityService {

    private final
    CategoryRepository categoryRepository;

    private final
    PriceRepository priceRepository;

    private final
    StatusRepository statusRepository;

    private final
    VendorRepository vendorRepository;

    private final
    BrandRepository brandRepository;

    private final
    PackRepository packRepository;

    private final
    CustomerRepository customerRepository;

    private final
    UserRepository userRepository;


    @Autowired
    public DefaultUtilityService(CategoryRepository categoryRepository, PackRepository packRepository, BrandRepository brandRepository, VendorRepository vendorRepository, PriceRepository priceRepository, StatusRepository statusRepository, CustomerRepository customerRepository, UserRepository userRepository) {
        this.categoryRepository = categoryRepository;
        this.packRepository = packRepository;
        this.brandRepository = brandRepository;
        this.vendorRepository = vendorRepository;
        this.priceRepository = priceRepository;
        this.statusRepository = statusRepository;
        this.customerRepository = customerRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<Category> getCategoryList() {
        return this.categoryRepository.findAll();
    }

    @Override
    public List<Price> getPriceList() {
        return this.priceRepository.findAll();
    }

    @Override
    public List<Status> getStatusList() {
        return this.statusRepository.findAll();
    }

    @Override
    public List<Brand> getBrandList() {
        return this.brandRepository.findAll();
    }

    @Override
    public List<Package> getPackageList() {
        return this.packRepository.findAll();
    }

    @Override
    public List<Vendor> getVendorList() {
        return this.vendorRepository.findAll();
    }

    @Override
    public ResponseEntity saveDependency(DependencyUtil util) {
        switch (util.getIndex()) {
            case 1: {
                Status status = new Status();
                status.setDescription(util.getDescription());
                return ResponseEntity.ok(this.statusRepository.save(status));
            }
            case 2: {
                Package pack = new Package();
                pack.setName(util.getDescription());
                return ResponseEntity.ok(this.packRepository.save(pack));
            }
            case 3: {
                Price price = new Price();
                price.setName(util.getDescription());
                return ResponseEntity.ok(this.priceRepository.save(price));
            }
            case 4: {
                Vendor vendor = new Vendor();
                vendor.setDescription(util.getDescription());
                return ResponseEntity.ok(this.vendorRepository.save(vendor));
            }
            case 5: {
                Category category = new Category();
                category.setName(util.getDescription());
                return ResponseEntity.ok(this.categoryRepository.save(category));
            }
            case 6: {
                Brand brand = new Brand();
                brand.setName(util.getDescription());
                return ResponseEntity.ok(this.brandRepository.save(brand));
            }
            default:
                return ResponseEntity.unprocessableEntity().build();
        }
    }

    @Override
    public ResponseEntity getClients(String term) {
        Pageable pageable = new PageRequest(0, 15, new Sort(Sort.Direction.DESC, "id"));
        Long id = 0L;
        try {
            id = new Long(term);
        } catch (Exception ignored) {
        }
        List<Customer> customers = this.customerRepository.findAllByNameContainingOrRncContainingOrPhoneContainingOrIdOrIdCardContaining(term, term, term, id, term, pageable).getContent();
        return ResponseEntity.ok(customers);
    }

    @Override
    public ResponseEntity getEmployees(String term) {
        Long id = 0L;
        try {
            id = new Long(term);
        } catch (Exception ignored) {
        }
        Pageable pageable = new PageRequest(0, 15, new Sort(Sort.Direction.DESC, "userId"));
        Page<User> userPage = this.userRepository
                .findAllByUsernameContainingOrEmailContainingOrFirstNameContainingOrLastNameContainingOrUserId(term, term, term, term, id, pageable);
        return ResponseEntity.ok(userPage.getContent());
    }

    @Override
    public Status getStatus(Long id) {
        return this.statusRepository.findOne(id);
    }

    @Override
    public User loggedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        if (username.equals("") || username.equals("anonymousUser"))
            throw new RuntimeException("User is not logged");
        return this.userRepository.findUser(username);
    }


}