package com.codespot.imperio.web.dto;

import com.codespot.imperio.model.entity.CashBox;
import com.codespot.imperio.model.entity.Customer;
import com.codespot.imperio.model.entity.PosProduct;
import com.codespot.imperio.model.enums.StatusType;
import com.codespot.imperio.security.model.User;
import com.codespot.imperio.service.sales.SalesService;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class InvoiceDto {

    private Long invoiceId;

    private Double tax;

    private String total;

    private String subTotal;

    private String discountTtl;

    private String discountPct;

    private Customer customer;

    private List<PosProduct> products;

    private Date date;

    private Date createdDate;

    private Date modifiedDate;

    private User modifiedBy;

    private User createdBy;

    private StatusType.Invoice status;

    private String voucherType;

    private String voucherNo;

    private String balance;

    private String credit;

    private Boolean isProcessed;

    private CashBox cashBox;

    private Integer payment;

    private String orderNo;

    private String paymentCondition;

    private String validity;

    private String shiftTime;

    private String procedureRef;

    private SalesService.ReportOrigin company;

}
