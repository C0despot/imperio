package com.codespot.imperio.web.dto;

import com.codespot.imperio.model.entity.ReceiptDocumentProduct;
import com.codespot.imperio.model.entity.util.Status;
import com.codespot.imperio.model.entity.util.Vendor;
import com.codespot.imperio.security.model.User;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ReceiptDocumentDto {

    private Long documentId;

    private Double total;

    private User holder;

    private User modifiedBy;

    private User createdBy;

    private Date date;

    private Date createdDate;

    private Date modificationDate;

    private Vendor vendor;

    private Boolean isFinished;

    private Status status;

    private List<ReceiptDocumentProduct> products;
}
