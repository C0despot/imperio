package com.codespot.imperio.web.controller;

import com.codespot.imperio.service.sales.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@RestController
@RequestMapping(value = "report")
public class ReportController {

    private final
    SalesService salesService;

    @Value("${RESOURCE_FOLDER_PATH:printconfig}")
    String path;

    @Autowired
    public ReportController(SalesService salesService) {
        this.salesService = salesService;
    }

    @GetMapping
    public ResponseEntity report(@RequestParam(name = "type") SalesService.ReportType type,
                                 @RequestParam(name = "origin", defaultValue = "RVIMP") SalesService.ReportOrigin origin,
                                 @RequestParam(name = "document") Long doc) throws IOException {
        try {
            salesService.printReport(type, path, doc);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok(path);
        }
        File file = new File(String.format("%s/%s.pdf", path, doc));
        byte[] contents = Files.readAllBytes(Paths.get(file.getPath()));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        return new ResponseEntity<>(contents, headers, HttpStatus.OK);
    }
}
