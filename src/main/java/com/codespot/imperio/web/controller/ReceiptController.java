package com.codespot.imperio.web.controller;


import com.codespot.imperio.service.audit.AuditService;
import com.codespot.imperio.util.PageUtil;
import com.codespot.imperio.util.SearchUtil;
import com.codespot.imperio.web.dto.ReceiptDocumentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping(value = "api/product")
public class ReceiptController {

    private final
    AuditService auditService;

    @Autowired
    public ReceiptController(AuditService auditService) {
        this.auditService = auditService;
    }

    @PostMapping(value = "receipt/document")
    ResponseEntity processDocument(@RequestBody ReceiptDocumentDto document, Principal principal) {
        return this.auditService.processReceiptDocument(document, principal.getName());
    }

    @GetMapping(value = "receipt/document/filter")
    ResponseEntity getDocument(PageUtil pageUtil, SearchUtil searchUtil) {
        return this.auditService.getReceiptDocuments(pageUtil, searchUtil);
    }

    @GetMapping(value = "receipt/document/{id}/extended")
    ResponseEntity getExtendedDocument(@PathVariable(value = "id") Long id) {
        return this.auditService.getExtendedReceiptDocument(id);
    }
}
