package com.codespot.imperio.web.controller;

import com.codespot.imperio.service.utility.UtilityService;
import com.codespot.imperio.util.DependencyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;


/**
 * @author - Jose A. Rodriguez.
 */

@RestController
@RequestMapping(value = "api/util")
public class UtilsController {

    private final
    UtilityService utilityService;

    @Autowired
    public UtilsController(UtilityService utilityService) {
        this.utilityService = utilityService;
    }

    @GetMapping(value = "category/all")
    public ResponseEntity getAllCategory() {
        return ResponseEntity.ok(this.utilityService.getCategoryList());
    }

    @GetMapping(value = "status/all")
    public ResponseEntity getAllStatus() {
        return ResponseEntity.ok(this.utilityService.getStatusList());
    }

    @GetMapping(value = "brand/all")
    public ResponseEntity getAllBrands() {
        return ResponseEntity.ok(this.utilityService.getBrandList());
    }

    @GetMapping(value = "pack/all")
    public ResponseEntity getAllPacks() {
        return ResponseEntity.ok(this.utilityService.getPackageList());
    }

    @GetMapping(value = "price/all")
    public ResponseEntity getAllPrice() {
        return ResponseEntity.ok(this.utilityService.getPriceList());
    }

    @GetMapping(value = "vendor/all")
    public ResponseEntity getAllVendor() {
        return ResponseEntity.ok(this.utilityService.getVendorList());
    }

    @PostMapping(value = {"", "/"})
    public ResponseEntity saveUtil(@RequestBody DependencyUtil dependencyUtil) {
        return this.utilityService.saveDependency(dependencyUtil);
    }

    @GetMapping(value = "customer")
    public ResponseEntity getClients(String term) {
        return this.utilityService.getClients(term);
    }

    @GetMapping(value = "employee")
    public ResponseEntity getEmployees(String term){
        return this.utilityService.getEmployees(term);
    }

    @GetMapping(value = "authorities")
    private ResponseEntity getAdminAuthority(Principal principal){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String roles = authentication.getAuthorities().toString().replace("[", "").replace("]", "");
        if (roles.contains("ADMINISTRADOR"))
            return ResponseEntity.status(200).build();
        return ResponseEntity.badRequest().build();
    }
}
