package com.codespot.imperio.web.controller;

import com.codespot.imperio.model.entity.*;
import com.codespot.imperio.model.enums.StatusType;
import com.codespot.imperio.service.sales.SalesService;
import com.codespot.imperio.util.PageUtil;
import com.codespot.imperio.util.SearchUtil;
import com.codespot.imperio.web.dto.InvoiceDto;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.text.ParseException;

@RestController
@RequestMapping(value = "api/sales")
@Log
public class SalesController {

    private final
    SalesService salesService;

    @Autowired
    public SalesController(SalesService salesService) {
        this.salesService = salesService;
    }

    @PostMapping(value = {"", "/"})
    public ResponseEntity processInvoice(@RequestBody InvoiceDto invoiceDto) {
        invoiceDto.getProducts().remove(invoiceDto.getProducts().size() - 1);
        StatusType.Invoice status;
        if (invoiceDto.getIsProcessed())
            status = StatusType.Invoice.PROCESSED;
        else status = StatusType.Invoice.DRAFTED;
        invoiceDto.setStatus(status);
        return this.salesService.process(invoiceDto);
    }

    @GetMapping(value = {"", "/"})
    public ResponseEntity getSales(PageUtil pageUtil, String searchBy, String criteria) {
        return this.salesService.getSales(pageUtil, searchBy, criteria);
    }

    @GetMapping(value = "one/{id}")
    public ResponseEntity getOne(@PathVariable(value = "id") Long id) {
        return this.salesService.getOne(id);
    }

    @PostMapping(value = "customer")
    public ResponseEntity saveCustomer(@RequestBody Customer customer) {
        customer = this.salesService.saveCustomer(customer);
        return ResponseEntity.ok(customer);
    }

    @PostMapping(value = "process/{id}")
    public ResponseEntity process(@PathVariable(value = "id") Long id) {
        return this.salesService.processDrafted(id);
    }

    @GetMapping(value = "customer/all")
    public ResponseEntity getAll(PageUtil pageUtil, SearchUtil searchUtil) {
        return this.salesService.getCustomers(pageUtil, searchUtil);
    }

    @GetMapping(value = "sqrt")
    public ResponseEntity sqrt(Principal principal) {
        return this.salesService.getSqrtNextStep(principal);
    }

    @PostMapping(value = "sqrt/{type}")
    public ResponseEntity open(@RequestBody OpenCashBox openCashBox,
                               @PathVariable(value = "type") String type) {
        openCashBox.setType(type);
        return this.salesService.openDay(openCashBox);
    }

    @GetMapping(value = "open/currently")
    public ResponseEntity currently() {
        return this.salesService.getCurrentlySqrt();
    }

    @PostMapping(value = "/process/cash/sqrt")
    public ResponseEntity processCash(@RequestBody CashBox cashBox) {
        return this.salesService.terminateCashBox(cashBox);
    }

    @GetMapping(value = "/cash/filter")
    public ResponseEntity getCashBoxes(PageUtil pageUtil, SearchUtil searchUtil) throws ParseException {
        return this.salesService.getBoxes(pageUtil, searchUtil);
    }

    @GetMapping(value = "/voucher")
    public ResponseEntity loadVoucher(SalesService.ReportOrigin origin) throws ParseException {
        return this.salesService.getVoucherInfo(origin);
    }

    @PostMapping(value = "/voucher")
    public ResponseEntity saveVoucher(@RequestBody CompanyVoucher voucher) throws ParseException {
        return this.salesService.saveVoucherInfo(voucher);
    }

    @PostMapping(value = "/conduce")
    public ResponseEntity createConduce(@RequestBody Conduce conduce) {
        return this.salesService.saveConduce(conduce);
    }

    @GetMapping(value = "/conduce")
    public ResponseEntity getConduce(@RequestParam Long invoice) {
        return this.salesService.getConduces(invoice);
    }
}
