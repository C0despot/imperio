package com.codespot.imperio.web.controller;

import com.codespot.imperio.model.ExcelProduct;
import com.codespot.imperio.model.entity.Product;
import com.codespot.imperio.service.product.ProductService;
import com.codespot.imperio.util.MassProductUpload;
import com.codespot.imperio.util.PageUtil;
import com.codespot.imperio.util.SearchUtil;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "api/product")
public class ProductController {

    public final
    ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(value = "all")
    public ResponseEntity getAll(PageUtil pageUtil) {
        return ResponseEntity.ok(this.productService.getProducts(pageUtil));
    }

    @PostMapping(value = "save")
    public ResponseEntity save(@RequestBody Product product, Principal principal) {
        return this.productService.saveProduct(product, principal.getName());
    }

    @GetMapping(value = "one/{productId}")
    public ResponseEntity getOne(@PathVariable(name = "productId") Long id) {
        Product product = this.productService.getById(id);
        if (product == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(product);
    }

    @PostMapping(value = "/image/{productId}")
    public ResponseEntity uploadImg(@RequestBody MultipartFile file,
                                    @PathVariable(value = "productId") Long productId, Principal principal) throws IOException {
        if (!file.isEmpty()) {
            String imageUrl = this.productService.changeProductImage(file, productId);
            Product product = this.productService.getById(productId);
            product.setImage(imageUrl);
            return this.productService.saveProduct(product, principal.getName());
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping(value = "search")
    public ResponseEntity search(PageUtil pageUtil, SearchUtil searchUtil) {
        return this.productService.search(pageUtil, searchUtil);
    }

    @GetMapping(value = "filter")
    @Secured(value = {"ADMINISTRADOR"})
    public ResponseEntity filter(String term) {
        return this.productService.filter(term);
    }


    @GetMapping(value = "upload")
    public ResponseEntity upload(@RequestParam String path) throws IOException, InvalidFormatException {

        List<ExcelProduct> products = MassProductUpload.readFromExcel(path);
        productService.massUpload(products);
        return ResponseEntity.ok(products);
    }
}


