package com.codespot.imperio.util;

import com.codespot.imperio.model.ExcelProduct;
import lombok.experimental.UtilityClass;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@UtilityClass
public class MassProductUpload {

    public List<ExcelProduct> readFromExcel(String path) throws IOException, InvalidFormatException {

        Workbook workbook = WorkbookFactory.create(new File(path));
        Sheet sheet = workbook.getSheetAt(0);
        List<ExcelProduct> products = new ArrayList<>();

        for (Row row : sheet) {
            ExcelProduct product = ExcelProduct
                    .builder()
                    .code(row.getCell(0).getStringCellValue().replace("                  ", ""))
                    .name(row.getCell(1).getStringCellValue().replace("                  ", "").split("''")[0])
                    .cost(row.getCell(2).getNumericCellValue())
                    .price(row.getCell(3).getNumericCellValue())
                    .amount(row.getCell(4).getNumericCellValue())
                    .category(row.getCell(5).getStringCellValue())
                    .build();
            products.add(product);
        }
        // Closing the workbook
        workbook.close();

        return products;

    }
}
