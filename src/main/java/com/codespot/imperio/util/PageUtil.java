package com.codespot.imperio.util;

import lombok.Data;

@Data
public class PageUtil {
    private int page;
    private int size;
}

