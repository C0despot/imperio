package com.codespot.imperio.util;

import lombok.Data;

@Data
public class DependencyUtil {
    private Integer index;
    private String description;
}
