package com.codespot.imperio.util;

import lombok.Data;

import java.util.Date;

@Data
public class SessionUtil {
    private Date creationTime;
    private String id;
    private Date lastAccessedTime;
    private long maxInactiveInterval;
    private Date thisAccessedTime;
}
