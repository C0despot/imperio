package com.codespot.imperio.util;

import lombok.Data;

@Data
public class SearchUtil {

    private String criteria;
    private String searchBy;
    private String order;
    private String startDate;
    private String endDate;

    public SearchUtil() {
        this.order = "DESC";
        this.startDate = "01-01-2000";
        this.endDate = "31-12-2049";
    }

    public SearchUtil(String searchBy, String criteria, String order) {
        this.criteria = criteria;
        this.searchBy = searchBy;
        this.order = order;
    }

    public SearchUtil(String searchBy, String criteria) {
        this.criteria = criteria;
        this.searchBy = searchBy;
        this.order = "DESC";
    }


    public SearchUtil(String criteria) {
        this.criteria = criteria;
        this.order = "DESC";
    }

    public void setStartDate(String startDate) {
        if (!startDate.equals(""))
            this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        if (!endDate.equals(""))
            this.endDate = endDate;
    }
}
