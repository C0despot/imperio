package com.codespot.imperio.util;

import lombok.Data;

@Data
public class SqrtUtil {

    private Integer days;

    public SqrtUtil(int days) {
        this.days = days;
    }

    public SqrtUtil() {
    }
}
