package com.codespot.imperio.print.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PrintableCustomer {
    protected int id;
    protected String firstName;
    protected String lastName;
    protected String street;
    protected String postalCode;
    protected String city;
    protected String countryId;
}
