package com.codespot.imperio.print.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PrintableProduct {
    protected int id;
    protected String name;
    protected double price;
    protected double vat;
}
