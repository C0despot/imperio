package com.codespot.imperio.print.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PrintableInvoice {
    private int id;
    private PrintableCustomer customer;
    private double total;
    private List<PrintableItem> items;
    private Date invoiceDate;
}
