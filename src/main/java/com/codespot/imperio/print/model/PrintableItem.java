package com.codespot.imperio.print.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PrintableItem {
    protected int item;
    protected PrintableProduct product;
    protected int quantity;
    protected double cost;
}
