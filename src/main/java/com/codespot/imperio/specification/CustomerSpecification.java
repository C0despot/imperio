package com.codespot.imperio.specification;

import com.codespot.imperio.model.entity.Customer;
import com.codespot.imperio.util.SearchUtil;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class CustomerSpecification implements Specification<Customer>{

    private SearchUtil searchUtil;

    public CustomerSpecification(SearchUtil searchUtil){
        this.searchUtil = searchUtil;
    }
    @Override
    public Predicate toPredicate(Root<Customer> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.like(
                root.get(searchUtil.getSearchBy()), "%" +searchUtil.getCriteria() + "%");
    }
}
