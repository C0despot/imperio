angular.module('app.ui').controller('EditCustomerCtrl', function ($scope, $http, $state, $stateParams, NotificationService) {

    $scope.customer = {
        clientType: '1',
        allowCredit: '1',
        phone: '',
        balance: 0
    };

    $scope.isLoading = 0;
    $scope.saveCustomer = function () {
        $scope.isLoading++;
        let customer = angular.copy($scope.customer);
        if (customer.maxCredit)
            customer.maxCredit = Number(customer.maxCredit.toString().replace(/,/g, ""));
        else customer.maxCredit = 0;
        $http.post('api/sales/customer', customer)
            .then(function (response) {
                NotificationService.logSuccess("Cliente Agregado");
                $state.reload();
            }, function (error) {
                NotificationService.logError("Algo Salio Mal")
            })
            .finally(function () {
                $scope.isLoading--;
            });
    };

    $scope.$on('$viewContentLoaded', function () {
        $scope.isLoading++;
        let id = $stateParams.id;
        if (!id)
            $state.go('dashboard');
        let url = `api/sales/customer/all?size=1&page=1&criteria=${id}`;
        $http.get(url)
            .then(function (response) {
                let content = response.data.content;
                if (content.length === 0) {
                    NotificationService.logError("Cliente no encontrado");
                    $scope.isLoading++;
                } else
                    $scope.customer = angular.copy(content[0]);
            }, function (error) {
                NotificationService.logError("Hubo Un problema al cargar el cliente.");
            })
            .finally(function () {
                $scope.isLoading--;
            });
    });
});
