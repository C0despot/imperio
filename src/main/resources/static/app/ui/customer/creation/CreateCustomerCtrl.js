angular.module('app.ui').controller('CreateCustomerCtrl', function ($scope, $http, $state, NotificationService) {

    $scope.customer = {
        clientType: '1',
        allowCredit: '1',
        phone: '',
        balance: 0
    };

    $scope.isLoading = 0;
    $scope.saveCustomer = function () {
        $scope.isLoading++;
        let customer = angular.copy($scope.customer);
        if (customer.maxCredit)
            customer.maxCredit = Number(customer.maxCredit.toString().replace(/,/g, ""));
        else customer.maxCredit = 0;
        $http.post('api/sales/customer', customer)
            .then(function (response) {
                NotificationService.logSuccess("Cliente Agregado");
                $state.reload();
            }, function (error) {
                NotificationService.logError("Algo Salio Mal")
            })
            .finally(function () {
                $scope.isLoading--;
            });
    };
});
