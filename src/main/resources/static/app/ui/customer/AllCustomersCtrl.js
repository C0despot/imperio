angular.module('app.ui').controller('AllCustomersCtrl', function ($scope, $http, NotificationService) {

    $scope.page = {
        size: 10,
        page: 1
    };

    $scope.isLoading = 0;
    $scope.criteria = '';
    $scope.customers = [];
    $scope.$on('$viewContentLoaded', function () {
        $scope.applyFilters();
    });

    $scope.applyFilters = function () {
        let url = `api/sales/customer/all?size=${$scope.page.size}&page=${$scope.page.page}&criteria=${$scope.criteria}`;
        $scope.isLoading++;
        $http.get(url)
            .then(function (response) {
                $scope.customers = response.data.content;
            }, function (error) {
                NotificationService.logError("Algo salio mal al intentar cargar los clientes.")
            })
            .finally(function () {
                $scope.isLoading--;
            });
    }
});