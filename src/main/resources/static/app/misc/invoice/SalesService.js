(function () {
    'use strict';
    angular.module('app.misc')
        .service('SalesService', SalesServices);
})();

SalesServices.$inject = ['$http'];

function SalesServices($http) {

    return {
        salesUrl: 'api/sales',
        utilUrl: 'api/util',
        printLocalService: 'http://localhost:60123/service/printer',
        getByFilter: function (page, queryString) {
            return $http.get(this.salesUrl + "?size=15&page=" + page + queryString);
        },
        saveInvoice: function (invoice) {
            return $http.post(this.salesUrl, invoice);
        },
        getCustomer: function (id) {
            return $http.get(this.utilUrl + '/customer?term=' + id);
        },
        getOne: function (id) {
            return $http.get(this.salesUrl + "/one/" + id);
        },
        printSale: function (invoice) {
            return $http.post(this.printLocalService + "/invoice", invoice);
        },
        processDrafted: function (id) {
            return $http.post(this.salesUrl + "/process/" + id);
        },
        doFullPrintSale: function (invoice) {
            return $http.post(this.printLocalService + "/invoice/full", invoice);
        },
        getEmployee: function (employee) {
            return $http.get(this.utilUrl + "/employee?term=" + employee);
        },
        sqrtNextStep: function () {
            return $http.get(this.salesUrl + "/sqrt");
        },
        sqrtOpenStep: function (obj) {
            return $http.post(this.salesUrl + "/sqrt/O", obj);
        },
        sqrtCloseStep: function (obj) {
            return $http.post(this.salesUrl + "/sqrt/C", obj);
        },
        processCashBox: function (obj) {
            return $http.post(this.salesUrl + "/process/cash/sqrt", obj);
        }
        ,
        currentOpeningCurrency: function () {
            return $http.get(this.salesUrl + "/open/currently");
        },
        getCashDocs:  function (page, queryString) {
            return $http.get(this.salesUrl + "/cash/filter?size=10&page=" + page + queryString);
        }
    }
}
