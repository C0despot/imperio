angular
    .module('app.misc')
    .controller('ProductDetailsModalCtrl', function ($scope, ProductService, NotificationService, productId, $uibModalInstance) {
        $scope.isLoading = 0;
        $scope.uibModalInstance = $uibModalInstance;
        $scope.product = null;
        setTimeout(function () {
            $scope.isLoading++;
            ProductService.getOne(productId)
                .then(function (response) {
                    $scope.isLoading--;
                    $scope.product = angular.copy(response.data);
                })
        }, 100);
    });
