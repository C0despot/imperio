angular.module('app.ui').controller('CustomerModalCtrl', function ($scope, $http, NotificationService, $uibModalInstance) {

    $scope.customer = {
        clientType: '1',
        allowCredit: '1',
        phone: '',
        balance: 0
    };

    $scope.uibModalInstance = $uibModalInstance;
    $scope.isLoading = 0;
    $scope.saveCustomer = function () {
        $scope.isLoading++;

        let customer = angular.copy($scope.customer);
        if (customer.maxCredit)
            customer.maxCredit = Number(customer.maxCredit.toString().replace(/,/g, ""));
        else customer.maxCredit = 0;
        $http.post('api/sales/customer', customer)
            .then(function (response) {
                NotificationService.logSuccess("Cliente Agregado");
                $uibModalInstance.close(response.data);
            }, function (error) {
                NotificationService.logError("Algo Salio Mal")
            })
            .finally(function () {
                $scope.isLoading--;
            });
    };
});
