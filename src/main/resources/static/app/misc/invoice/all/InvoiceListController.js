angular.module('app.misc').controller('InvoiceListController', function ($scope, $filter, $state, SalesService, NotificationService) {

    $scope.isLoading = 0;
    $scope.pager = {
        index: 1,
        totalPages: 1
    };
    $scope.criteria = '';
    $scope.invoiceList = [];
    $scope.$on('$viewContentLoaded', function () {
        // var filterUrl = '&searchBy='+$scope.searchBy + "&criteria="+ $scope.criteria;
        var filterUrl = '&searchBy=client'+ "&criteria="+ $scope.criteria;
        $scope.isLoading++;
        SalesService.getByFilter(0, filterUrl)
            .then(function (response) {
                $scope.invoiceList = response.data;
                $scope.pager.totalPages = response.data.totalPages;
            }, function (error) {
                NotificationService.logError("La busqueda no trajo resultados.")
            })
            .finally(function () {
                $scope.isLoading--;
            });
    });
    $scope.setPager = function (value) {
        if(value > $scope.pager.totalPages )
            value = $scope.pager.totalPages;
        else if(value < 1)
            value = 1;
        else if (typeof value === 'undefined') {
            value = $scope.pager.totalPages;
        }
        $scope.pager.index = value;
        $scope.pageHelper = value;
        $scope.isLoading++;
        $scope.getByPageAndFilter();
    };
    $scope.applyFilters = function () {
        $scope.isLoading++;
        $scope.pager.index = 1;
        $scope.pageHelper = 1;
        $scope.getByPageAndFilter()
    };

    $scope.getByPageAndFilter = function () {
        var filterUrl = '&searchBy='+$scope.searchBy + "&criteria="+ $scope.criteria;
        SalesService.getByFilter($scope.pager.index - 1, filterUrl)
            .then(function (response) {
                $scope.invoiceList = response.data;
                $scope.pager.totalPages = response.data.totalPages;
            }, function (error) {
                NotificationService.logError("La busqueda no trajo resultados.")
            })
            .finally(function () {
                $scope.isLoading--;
            });
    }

    $scope.searchBy = 'client';
    $scope.searchObject = {
        client: 'Cliente',
        invoiceId : 'Numero de Factura',
        rnc: 'RNC',
        date: 'Fecha'
    };

    $(".dropdown-menu a").click(function() {
        $(this).closest(".dropdown-menu").prev().dropdown("toggle");
    });
});