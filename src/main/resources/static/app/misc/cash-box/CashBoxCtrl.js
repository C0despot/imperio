angular.module('app.misc')
    .controller('CashBoxCtrl', function ($scope, $interval, $filter, ProductService, SalesService, $uibModal, NotificationService) {

        $scope.receiptDocuments = [];
        $scope.isLoading = 0;
        $scope.criteria = '';

        $scope.pager = {
            index: 0,
            totalPages: 1
        };
        $scope.filter = {
            startDate: '',
            endDate: ''
        };

        $scope.$on('$viewContentLoaded', function () {
            $scope.getByPageAndFilter();
        });

        $scope.preApplyFilter = function () {
            $scope.getByPageAndFilter();
        };
        $scope.getByPageAndFilter = function () {
            let filterUrl = `&searchBy=${$scope.searchBy}&criteria=${$scope.criteria}&startDate=${$scope.filter.startDate}&endDate=${$scope.filter.endDate}`;
            $scope.isLoading++;
            SalesService.getCashDocs($scope.pager.index, filterUrl)
                .then(function (response) {
                    $scope.receiptDocuments = angular.copy(response.data.content);
                    $scope.pager.totalPages = response.data.totalPages;
                }, function (error) {
                    NotificationService.logError("La busqueda no trajo resultados.")
                })
                .finally(function () {
                    $scope.isLoading--;
                });
        };

        $scope.setPager = function (value) {
            if (value >= $scope.pager.totalPages)
                value = $scope.pager.totalPages - 1;
            else if (value < 0)
                value = 0;
            else if (typeof value === 'undefined') {
                value = $scope.pager.totalPages - 1;
            }
            $scope.pager.index = value;
            $scope.pageHelper = value + 1;
            $scope.getByPageAndFilter();
        };

        $scope.viewDetails = function (cashBox) {
            $uibModal.open({
                controller: 'CloseSalesModalCtrl',
                templateUrl: 'app/dashboard/sqrt/close.html',
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    scripts: function (lazyScript) {
                        return lazyScript.register([
                            'build/vendor.datatables.js',
                            'build/vendor.ui.js'
                        ]);

                    },
                    cashBox: function () {
                        return cashBox;
                    }
                }
            });
        };

        $scope.searchObject = {
            product: 'Producto',
            supplier: 'Suplidor',
            date: 'Fecha'
        };
        $scope.searchBy = 'product';

        $(".dropdown-menu a").click(function () {
            $(this).closest(".dropdown-menu").prev().dropdown("toggle");
        });

    });