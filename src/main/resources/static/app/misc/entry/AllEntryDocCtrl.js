angular
    .module('app.misc')
    .controller('AllEntryDocCtrl', function ($scope, $filter, $uibModal, ProductService, SalesService, $state, NotificationService) {

        $scope.receiptDocuments = [];
        $scope.isLoading = 0;
        $scope.criteria = '';

        $scope.pager = {
            index: 0,
            totalPages: 1
        };

        $scope.$on('$viewContentLoaded', function () {
            $scope.getByPageAndFilter();
        });

        $scope.getByPageAndFilter = function () {
            let filterUrl = '&searchBy=' + $scope.searchBy + "&criteria=" + $scope.criteria;
            $scope.isLoading++;

            ProductService.getReceiptDocument($scope.pager.index, filterUrl)
                .then(function (response) {
                    $scope.receiptDocuments = angular.copy(response.data.content);
                    $scope.pager.totalPages = response.data.totalPages;
                }, function (error) {
                    NotificationService.logError("La busqueda no trajo resultados.")
                })
                .finally(function () {
                    $scope.isLoading--;
                });
        };

        $scope.setPager = function (value) {
            if (value > $scope.pager.totalPages)
                value = $scope.pager.totalPages;
            else if (value < 1)
                value = 1;
            else if (typeof value === 'undefined') {
                value = $scope.pager.totalPages;
            }
            $scope.pager.index = value - 1;
            $scope.pageHelper = value;
            $scope.getByPageAndFilter();
        };
        $scope.searchObject = {
            product: 'Producto',
            supplier: 'Suplidor',
            date: 'Fecha'
        };
        $scope.searchBy = 'product';

        $(".dropdown-menu a").click(function () {
            $(this).closest(".dropdown-menu").prev().dropdown("toggle");
        });
    });
