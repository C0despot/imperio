angular
    .module('app.misc')
    .controller('NewEntryCtrl', function ($scope, $interval, $filter, $uibModal, ProductService, SalesService, $state, NotificationService) {

        $scope.isLoading = 0;
        $scope.processed = false;
        $scope.employee = null;
        $scope.allowSave = true;

        $scope.document = {
            total: '0.00',
            date: new Date(),
            holder: null,
            isFinished: $scope.isFinished,
            products: [{
                qty: 1,
                price: 0.0,
                id: null,
                description: null,
                salePrice: 0.0,
                gain: 0.0
            }],
            vendor: null
        };

        let validateDocumentForm = function () {

            let doc = angular.copy($scope.document);

            if (doc.holder === null)
                return [false, "Ingrese quien entrego el pedido"];
            else if (doc.products.length === 1)
                return [false, "Debe seleccionar al menos un producto"];
            else if (doc.vendor === null)
                return [false, "Debe seleccionar el suplidor de la lista"];
            else {
                for (let i = 0; i < doc.products.length - 1; i++) {
                    let p = doc.products[i];
                    if (Number(p.qty) <= 0)
                        return [false, `Revise la cantidad de ${p.description}`];
                    else if (p.id === null)
                        return [false, `Revise el producto ${p.description}`];
                    else if (p.salePrice <= 0)
                        return [false, `Hubo un error con el producto ${p.description}`];
                }
                return [true];
            }
        };
        $scope.process = function () {
            let document = angular.copy($scope.document);

            let validate = angular.copy(validateDocumentForm());
            if (!validate[0]) {
                NotificationService.logError(validate[1]);
                return;
            }
            document.total = document.total.toString().replace(/,/g, "");
            document.products.forEach(function (product) {
                if (product.price)
                    product.price = product.price.toString().replace(/,/g, "");
                if (product.salePrice)
                    product.salePrice = product.salePrice.toString().replace(/,/g, "");
            });
            $scope.isLoading++;
            ProductService
                .processReceipt(document)
                .then(function (response) {
                    NotificationService.logSuccess("Cambios Guardados..!");
                    $state.reload();
                }, function (error) {
                    NotificationService.logError("Algo Salio Mal.");
                })
                .finally(function () {
                    $scope.isLoading--;
                });
        };


        $scope.removeRow = function (index) {
            if (index !== $scope.document.products.length - 1)
                $scope.document.products.splice(index, 1);
        };

        $scope.setEmployee = function (employee) {
            $scope.isLoading++;
            SalesService
                .getEmployee(employee)
                .then(function (response) {
                    $scope.document.holder = angular.copy(response.data[0]);
                }, function (error) {
                    NotificationService.logError("Algo Salio Mal al cargar el usuario");
                })
                .finally(function () {
                    $scope.isLoading--;
                })
        };
        $scope.vendorList = [];
        $scope.fakeDate = $filter("date")($scope.document.date, "dd/MM/yyyy 'a las' hh:mm:ss a");

        $scope.$on('$viewContentLoaded', function () {
            ProductService
                .validateDiscount()
                .then(function (response) {
                }, function (error) {
                    $state.go("app.dashboard");
                    NotificationService.logError("No Permitido");
                });
            ProductService.getVendor()
                .then(function (response) {
                    $scope.vendorList = angular.copy(response.data)
                });
        });

        $scope.setSalePrice = function (index, productId) {
            if (productId) {
                $scope.isLoading++;
                ProductService
                    .getOne(productId)
                    .then(function (response) {
                        let gain = response.data.gain;
                        let newPrice = $scope.document.products[index].price;
                        let priceType = response.data.priceType;
                        switch (priceType.id) {
                            case 1000000000:
                                let salePrice = Number((newPrice.toString().replace(/,/g, "") * (1 + gain / 100)).toFixed(2));
                                $scope.document.products[index].salePrice = salePrice;
                                break;
                            default:
                                let sp = response.data.salePrice;
                                let pp = response.data.purchasePrice;
                                $scope.document.products[index].salePrice = Number((sp - pp) + newPrice * 1).toFixed(2);
                                break;
                        }
                        $scope.document.products[index].gain = angular.copy(gain);
                    }, function (error) {
                        NotificationService.logError("Algo Salio Mal");
                    }).finally(function () {
                    $scope.isLoading--;
                });
            }
        };

        setInterval(function () {
            $scope.document.total = ProductService.sum($scope.document.products, 'price');
        }, 50);

        $scope.details = function (id) {
            $uibModal.open({
                controller: 'ProductDetailsModalCtrl',
                templateUrl: 'app/misc/invoice/modal/viewProductDetails.html',
                backdrop: 'static',
                keyboard: false,
                windowClass: 'window-modal-lg',
                resolve: {
                    productId: function () {
                        return id;
                    }
                }
            });
        };
        $scope.addProduct = function (value, index) {
            let row = $scope.document.products[index];
            $scope.isLoading++;
            ProductService
                .getOne(value)
                .then(function (response) {
                    $scope.isLoading--;
                    let product = response.data;
                    row.price = product.purchasePrice;
                    row.code = product.code;
                    row.description = product.name.toUpperCase();
                    if ($scope.document.products.length === index + 1)
                        $scope.addRow();
                    $scope.setSalePrice(index, value);

                }, function (error) {
                    $scope.isLoading--;
                    NotificationService.logError("Fallo Al Cargar Informacion Del Producto")
                });
        };

        $scope.open = function (index) {
            let modalInstance = $uibModal.open({
                controller: 'NewProductModalCtrl',
                templateUrl: 'app/misc/invoice/modal/addProductModal.html',
                backdrop: 'static',
                keyboard: false,
                windowClass: 'window-modal-lg',
                resolve: {
                    isNew: function () {
                        return true;
                    },
                    isEntry: function () {
                        return true;
                    }
                }
            });
            modalInstance.result.then(function (result) {
                if (result !== null)
                    $("#select2-" + index).select2('data', {id: result.id, text: result.name}).trigger('change');
            })
        };

        $scope.addRow = function () {
            $scope.document.products.push({
                qty: 1,
                price: 0.0,
                id: null,
                description: null,
                code: null
            });

            setTimeout(function () {
                $(".product-search").select2({
                    minimumInputLength: 0,
                    ajax: {
                        url: '/api/product/filter',
                        dataType: 'json',
                        quietMillis: 100,
                        data: function (term) {
                            return {
                                term: term
                            };
                        },
                        results: function (data) {
                            let results = [];
                            $.each(data, function (index, item) {
                                results.push({
                                    id: item.id,
                                    text: item.name.toUpperCase()
                                });
                            });
                            return {
                                results: results
                            };
                        }
                    }
                });
                for (let i = 0; i < $scope.document.products.length - 1; i++) {
                    let t = angular.copy($scope.document.products[i]);
                    if (t.id === null || t.id === undefined)
                        break;
                    $("#select2-" + i).select2('data', {id: t.id, text: t.description}).trigger('change');
                }
            }, 10);
        }
    });