angular.module('app.misc').controller('CreateQuoteController', function ($scope, $filter, $state, $uibModal, SalesService, ProductService, NotificationService) {

    $scope.isLoading = 0;
    $scope.processed = false;
    $scope.invoice = {
        tax: '0.00',
        payment: 1001,
        voucherType: 1001,
        total: '0.00',
        subTotal: '0.00',
        discountTtl: '0.00',
        date: new Date(),
        customer: null,
        isProcessed: $scope.processed,
        discountPct: '0.00',
        products: [{
            qty: 1,
            price: 0.0,
            id: null,
            description: null,
            code: null
        }]
    };

    $scope.paymentList = [
        {
            index: 1001,
            description: "Efectivo"
        },
        {
            index: 1010,
            description: "Tarjeta"
        },
        {
            index: 1100,
            description: "Cheque"
        },
        {
            index: 1101,
            description: "Credito"
        }
    ];

    $scope.voucherList = [
        {
            index: 1001,
            description: "Consumidor Final"
        },
        {
            index: 1010,
            description: "Credito Fiscal"
        },
        {
            index: 1100,
            description: "Gubernamental"
        },
        {
            index: 1101,
            description: "Nota de Credito"
        },
        {
            index: 1110,
            description: "Especial"
        }
    ];
    $scope.state = $state;
    $scope.open = function (index) {
        let modalInstance = $uibModal.open({
            controller: 'NewProductModalCtrl',
            templateUrl: 'app/misc/invoice/modal/addProductModal.html',
            backdrop: 'static',
            keyboard: false,
            windowClass: 'window-modal-lg',
            resolve: {
                isNew: function () {
                    return true;
                },
                isEntry: function () {
                    return false;
                }
            }
        });
        modalInstance.result.then(function (result) {
            if (result !== null)
                $("#select2-" + index).select2('data', {id: result.id, text: result.name}).trigger('change');
        })
    };
    $scope.openCustom = function () {
        let modalInstance = $uibModal.open({
            controller: 'CustomerModalCtrl',
            templateUrl: 'app/misc/invoice/modal/addCustomerModal.html',
            backdrop: 'static',
            keyboard: false,
            windowClass: 'window-modal-lg',
            resolve: {
                isNew: function () {
                    return true;
                }
            }
        });
        modalInstance.result.then(function (result) {
            if (result !== null)
                $("#client").select2('data', {id: result.id, text: result.name.toUpperCase()}).trigger('change');
        })
    };

    $scope.details = function (id) {
        $uibModal.open({
            controller: 'ProductDetailsModalCtrl',
            templateUrl: 'app/misc/invoice/modal/viewProductDetails.html',
            backdrop: 'static',
            keyboard: false,
            windowClass: 'window-modal-lg',
            resolve: {
                productId: function () {
                    return id;
                }
            }
        });
    };
    setInterval(function () {
        $scope.invoice.subTotal = $scope.sum($scope.invoice.products, 'price');
        $scope.invoice.discountTtl = $scope.invoice.subTotal * ($scope.invoice.discountPct / 100);
        $scope.invoice.tax = ($scope.invoice.subTotal - $scope.invoice.discountTtl) * (0.18);
        $scope.invoice.total = ($scope.invoice.subTotal - $scope.invoice.discountTtl + $scope.invoice.tax);
    }, 50);

    $scope.setClient = function (id) {
        $scope.isLoading++;
        SalesService
            .getCustomer(id)
            .then(function (response) {
                $scope.isLoading--;
                $scope.invoice.customer = response.data[0];
            }, function (error) {
                $scope.isLoading--;
            });
    };

    $scope.validateTotal = function (index, productId) {
        let qty = $scope.invoice.products[index].qty;
        if (productId) {
            $scope.isLoading++;
            ProductService
                .getOne(productId)
                .then(function (response) {
                    let av = response.data.totalAmount;
                    if (qty > av) {
                        $scope.invoice.products[index].qty = angular.copy(av);
                        NotificationService.logError("Ha excedido el maximo de productos disponibles [" + av + "]")
                    }
                }, function (error) {
                    NotificationService.logError("Algo Salio Mal");
                }).finally(function () {
                $scope.isLoading--;
            });
        }
    };
    $scope.process = function () {
        $scope.invoice.isProcessed = $scope.processed;
        if ($scope.invoice.products.length === 1)
            NotificationService.logError("Debe Seleccionar Al Menos Un Producto");
        else {
            $scope.isLoading++;
            SalesService
                .saveInvoice($scope.invoice)
                .then(function (response) {
                    $scope.isLoading--;
                    NotificationService.logSuccess("Procesado, cargando vista previa.");
                    $state.go('invoicePreview', {id: response.data.invoiceId});
                }, function (error) {
                    NotificationService.logError(error.data.message);
                    $scope.isLoading--;
                })
        }
    };
    $scope.sum = function (items, prop) {
        return items.reduce(function (a, b) {
            return a + b[prop] * b ['qty'];
        }, 0);
    };

    $scope.validateDiscount = function () {
        ProductService.validateDiscount()
            .then(function (ignore) {
            }, function (error) {
                if ($scope.invoice.discountPct > 10)
                    $scope.invoice.discountPct = 10;
            });
    };
    $scope.$on('$viewContentLoaded', function () {
    });

    $scope.fakeDate = $filter("date")($scope.invoice.date, "dd/MM/yyyy, hh:mm:ss a");

    $scope.addProduct = function (value, index) {
        let row = $scope.invoice.products[index];
        $scope.isLoading++;
        ProductService
            .getOne(value)
            .then(function (response) {
                $scope.isLoading--;
                let product = response.data;
                if (product.totalAmount === 0)
                    NotificationService.logError(product.name + " Agotado.");
                else if (product.totalAmount < row.qty) {
                    row.price = response.data.salePrice;
                    row.description = response.data.name.toUpperCase();
                    if ($scope.invoice.products.length === index + 1)
                        $scope.addRow();
                    row.qty = product.totalAmount;
                    row.code = product.code;
                    NotificationService.logError("Cantidad No Suficiente, " + product.name + ". [" + product.totalAmount + "] Disponibles")
                } else {
                    row.price = product.salePrice;
                    row.code = product.code;
                    row.description = product.name.toUpperCase();
                    if ($scope.invoice.products.length === index + 1)
                        $scope.addRow();
                }
            }, function (error) {
                $scope.isLoading--;
                NotificationService.logError("Fallo Al Cargar Informacion Del Producto")
            });
    };

    $scope.removeRow = function (index) {
        if (index !== $scope.invoice.products.length - 1)
            $scope.invoice.products.splice(index, 1);
    };

    $scope.addRow = function () {
        $scope.invoice.products.push({
            qty: 1,
            price: 0.0,
            id: null,
            description: null
        });

        setTimeout(function () {
            $(".product-search").select2({
                minimumInputLength: 0,
                ajax: {
                    url: '/api/product/filter',
                    dataType: 'json',
                    quietMillis: 100,
                    data: function (term) {
                        return {
                            term: term
                        };
                    },
                    results: function (data) {
                        let results = [];
                        $.each(data, function (index, item) {
                            results.push({
                                id: item.id,
                                text: item.name.toUpperCase()
                            });
                        });
                        return {
                            results: results
                        };
                    }
                }
            });
            for (let i = 0; i < $scope.invoice.products.length - 1; i++) {
                let t = angular.copy($scope.invoice.products[i]);
                if (t.id === null || t.id === undefined)
                    break;
                $("#select2-" + i).select2('data', {id: t.id, text: t.description}).trigger('change');
            }

        }, 10);
    }
});
