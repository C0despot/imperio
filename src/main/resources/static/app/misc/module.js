"use strict";

angular.module('app.misc', ['ui.router']);


angular.module('app.misc').config(function ($stateProvider) {

    $stateProvider
        .state('app.misc', {
            abstract: true,
            data: {
                title: 'Ventas'
            }
        })

        .state('app.misc.pricingTable', {
            url: '/pricing-table',
            data: {
                title: 'Pricing Table'
            },
            views: {
                "content@app": {
                    templateUrl: 'app/misc/views/pricing-table.html'
                }
            }
        })

        .state('app.misc.newInvoice', {
            url: '/invoice/new',
            views: {
                "content@app": {
                    templateUrl: 'app/misc/invoice/invoice.html',
                    controller: 'NewInvoiceCtrl'
                }
            },
            data: {
                title: 'Nueva Factura'
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.datatables.js',
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })
        .state('app.misc.editInvoice', {
            url: '/invoice/:id/edition',
            views: {
                "content@app": {
                    templateUrl: 'app/misc/invoice/invoice.html',
                    controller: 'EditInvoiceCtrl'
                }
            },
            data: {
                title: 'Edición Cotización'
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.datatables.js',
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })
        .state('app.misc.newEntry', {
            url: '/entry/new',
            views: {
                "content@app": {
                    templateUrl: 'app/misc/entry/entry.html',
                    controller: 'NewEntryCtrl'
                }
            },
            data: {
                title: 'Nuevo Ingreso'
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.datatables.js',
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })
        .state('app.misc.editEntry', {
            url: '/entry/:id/edition',
            views: {
                "content@app": {
                    templateUrl: 'app/misc/entry/entry.html',
                    controller: 'EditEntryCtrl'
                }
            },
            data: {
                title: 'Editar Ingreso'
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.datatables.js',
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })
        .state('app.misc.cashBoxList', {
            url: '/cash/close/all',
            views: {
                "content@app": {
                    templateUrl: 'app/misc/cash-box/cash-box.html',
                    controller: 'CashBoxCtrl'
                }
            },
            data: {
                title: 'Lista de Cuadres'
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.datatables.js',
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })
        .state('app.misc.allEntries', {
            url: '/entry/all',
            views: {
                "content@app": {
                    templateUrl: 'app/misc/entry/all.html',
                    controller: 'AllEntryDocCtrl'
                }
            },
            data: {
                title: 'Ingreso de Productos'
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.datatables.js',
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })
        .state('app.misc.invoiceList', {
            url: '/invoice/all',
            views: {
                "content@app": {
                    templateUrl: 'app/misc/invoice/all/invoice.list.html',
                    controller: 'InvoiceListController'
                }
            },
            data: {
                title: 'Ventas Realizadas'
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.datatables.js',
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })


        .state('app.misc.invoice', {
            url: '/invoice',
            data: {
                title: 'Factura'
            },
            views: {
                "content@app": {
                    templateUrl: 'app/misc/views/invoice.html'
                }
            }
        })

        .state('app.misc.quote', {
            url: '/quotes',
            data: {
                title: 'Cotizaciones'
            },
            views: {
                "content@app": {
                    templateUrl: 'app/misc/quote/quote.html',
                    controller: 'CreateQuoteController'
                }
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.datatables.js',
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })

        .state('app.misc.error404', {
            url: '/404',
            data: {
                title: 'Error 404'
            },
            views: {
                "content@app": {
                    templateUrl: 'app/misc/views/error404.html'
                }
            }
        })

        .state('app.misc.error500', {
            url: '/500',
            data: {
                title: 'Error 500'
            },
            views: {
                "content@app": {
                    templateUrl: 'app/misc/views/error500.html'
                }
            }
        })


        .state('app.misc.blank', {
            url: '/blank',
            data: {
                title: 'Blank'
            },
            views: {
                "content@app": {
                    templateUrl: 'app/misc/views/blank.html'
                }
            }
        })

        .state('app.misc.test', {
            url: '/test',
            data: {
                title: 'Test'
            },
            views: {
                "content@app": {
                    templateUrl: 'app/misc/views/test.html'
                }
            }
        })

        .state('app.misc.emailTemplate', {
            url: '/email-template',
            data: {
                title: 'Email Template'
            },
            views: {
                "content@app": {
                    templateUrl: 'app/misc/views/email-template.html'
                }
            }
        })

        .state('app.misc.search', {
            url: '/search',
            data: {
                title: 'Search'
            },
            views: {
                "content@app": {
                    templateUrl: 'app/misc/views/search.html'
                }
            }
        })

        .state('app.misc.ckeditor', {
            url: '/ckeditor',
            data: {
                title: 'CK Editor'
            },
            views: {
                "content@app": {
                    templateUrl: 'app/misc/views/ckeditor.html'
                }
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register('smartadmin-plugin/legacy/ckeditor/ckeditor.js');
                }
            }
        })
});
