'use strict';

angular.module('app.auth').factory('User', function ($http, $state) {
    return {
        userDetails: null,
        doLogin: function (credentials) {
            return $http({
                method: 'POST',
                url: 'login',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function (obj) {
                    let str = [];
                    for (let p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {username: credentials.username, password: credentials.password}
            });
        },
        refresh: function () {
            let dis = this;
            $http.get("user")
                .then(function (response) {
                    dis.userDetails = response.data;
                }, function () {
                    $state.go("login")
                });
        },
        getRoles: function () {
            return $http.get("user/roles");
        },
        save : function (user) {
            return $http.post("user", user);
        },
        validate : function (user) {
            return $http.post("user/validate", user);
        },
        byToken : function (token) {
            return $http.post("user/token", token);
        },
        getMe: function () {
            return $http.get("user/me");
        }
    }
});
