"use strict";

angular.module('app.auth', [
    'ui.router'
//        ,
//        'ezfb',
//        'googleplus'
]).config(function ($stateProvider) {
//        GooglePlusProvider.init({
//            clientId: authKeys.googleClientId
//        });
//
//        ezfbProvider.setInitParams({
//            appId: authKeys.facebookAppId
//        });
    $stateProvider.state('realLogin', {
        url: '/real-login',

        views: {
            root: {
                templateUrl: "app/auth/login/login.html",
                controller: 'LoginCtrl'
            }
        },
        data: {
            title: 'Login',
            rootId: 'extra-page'
        }

    })

        .state('login', {
            url: '/',
            views: {
                root: {
                    templateUrl: "app/auth/login/login.html",
                    controller: 'LoginCtrl'
                }
            },
            data: {
                title: 'Login',
                htmlId: 'extr-page'
            },
            resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })
        .state('register', {
            url: '/register',
            views: {
                root: {
                    templateUrl: 'app/auth/views/register.html'
                }
            },
            data: {
                title: 'Register',
                htmlId: 'extr-page'
            }
        })
        .state('invoicePreview', {
            url: '/invoice/:id/preview',
            views: {
                root: {
                    templateUrl: 'app/auth/invoice/preview/preview.html',
                    controller: 'InvoicePreviewCtrl'
                }
            },
            data: {
                title: 'Vista de Factura',
                htmlId: 'extr-page'
            }
        })
        .state('lock', {
            url: '/:token/welcome',
            views: {
                root: {
                    templateUrl: 'app/auth/views/lock.html',
                    controller: 'ValidationCtrl'
                }
            },
            data: {
                title: 'Locked Screen',
                htmlId: 'lock-page'
            }
        })
        //
        // .state('landing', {
        //     url: '/',
        //     views: {
        //         root: {
        //             templateUrl: 'app/landing/index.html'
        //         }
        //     },
        //     data: {
        //         title: 'Portal Web'
        //     }
        // })
        .state('printer', {
            url: '/printer',
            views: {
                root: {
                    templateUrl: 'app/auth/printer/printer.html',
                    controller: 'PrinterCtrl'
                }
            },
            data: {
                title: 'Printer Configuration',
                htmlId: 'print-page'
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.datatables.js',
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })
        .state('voucher', {
            url: '/voucher',
            views: {
                root: {
                    templateUrl: 'app/auth/voucher/voucher.html',
                    controller: 'VoucherCtrl'
                }
            },
            data: {
                title: 'Voucher Configuration',
                htmlId: 'print-page'
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.datatables.js',
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })

}).constant('authKeys', {
    googleClientId: '',
    facebookAppId: ''
});
