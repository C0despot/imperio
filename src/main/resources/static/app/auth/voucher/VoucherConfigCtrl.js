angular.module('app.auth')
    .controller('VoucherCtrl', function ($scope, $state, $http, User, NotificationService) {

        $scope.voucher = {};

        $scope.$on('$viewContentLoaded', function () {
            $(".fa-arrow-circle-left").click(function (event) {
                event.preventDefault();
                window.history.back();
            });
        });

        $scope.saveVoucher = function () {
            $http.post(`api/sales/voucher`, $scope.voucher)
                .then(function (response) {
                    $scope.voucher = response.data;
                    NotificationService.logSuccess("Se ha guardado la configuracion de comprobantes.")
                }, function (error) {
                    NotificationService.logError("Algo Salio mal al guardar la informacion de impresion.")
                })
                .finally(function () {
                    $scope.isLoading--;
                })
        };

        $scope.loadConfig = (company) => {
            $scope.isLoading++;
            $http.get(`api/sales/voucher?origin=${company}`)
                .then(function (response) {
                    $scope.voucher = response.data;
                }, function (error) {
                    NotificationService.logError("Algo Salio mal al guardar la informacion de impresion.")
                })
                .finally(function () {
                    $scope.isLoading--;
                })
        }
    });
