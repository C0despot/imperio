"use strict";

angular.module('app.auth').controller('LoginCtrl', function ($scope, $state, User, NotificationService) {

    $scope.user = {
        username: '',
        password: ''
    };
    $scope.isLoading = 0;

    $scope.doLogin = function () {
        $scope.isLoading ++;
        User.doLogin($scope.user)
            .then(function (response) {
                User.refresh();
                $state.go('app.dashboard')
            }, function (error) {
                NotificationService.logError("Nombre de Usuario ó Contrasena Incorrectos")
            })
            .finally(function () {
                $scope.isLoading--;
            });
    }

});
