angular.module('app.calendar')
    .controller('ValidationCtrl', function ($scope, $state, $stateParams, User, NotificationService) {

        $scope.isLoading = 0;
        $scope.user = null;
        $scope.$on('$viewContentLoaded', function () {
            $scope.isLoading++;
            User.byToken($stateParams.token)
                .then(function (response) {
                    $scope.user = response.data;
                }, function () {
                    NotificationService.logError("Algo salio mal, al intentar cargar el usuario.")
                })
                .finally(function () {
                    $scope.isLoading--;
                })
        });

        $scope.validateUser = function () {
            $scope.isLoading++;
            User.validate($scope.user)
                .then(function (response) {
                    NotificationService.logSuccess("Bienvenindo..!");
                    $state.go("login");
                }, function (error) {
                    $scope.user = {};
                    NotificationService.logError("Algo Salio Mal");
                })
                .finally(function () {
                    $scope.isLoading--;
                })
        };
    });