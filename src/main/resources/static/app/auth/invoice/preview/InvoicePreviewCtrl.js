angular.module('app.auth').controller('InvoicePreviewCtrl', function ($scope, $stateParams, $state, SalesService, NotificationService) {

    $scope.origin = 'RVIMP';
    $scope.invoice = {
        invoiceId: '',
        tax: '0.00',
        total: '0.00',
        subTotal: '0.00',
        discountTtl: '0.00',
        date: new Date(),
        customer: null,
        discountPct: '0.00',
        products: [{
            qty: 1,
            price: 0.0,
            id: null,
            description: null,
            code: null
        }]
    };
    $scope.$on('$viewContentLoaded', function () {
        $(".fa-arrow-circle-left").click(function (event) {
            event.preventDefault();
            window.history.back();
        });
        $scope.isLoading++;
        SalesService.getOne($stateParams.id)
            .then(function (response) {
                $scope.invoice = angular.copy(response.data);
            }, function (error) {
                NotificationService.logError("No se ha podido cargar el documento seleccionado. Intente Nuevamente")
            })
            .finally(function () {
                $scope.isLoading--;
            })
    });

    $scope.doPrint = function () {
        SalesService.printSale($scope.invoice)
            .then(function (response) {
                NotificationService.logSuccess("Imprimiendo...")
            }, function (error) {
                NotificationService.logError("Hubo Un Problema con la impresion.")
            });
    };

    $scope.doNormalPrint = function () {
    };
    $scope.doProcess = function () {
        SalesService
            .processDrafted($scope.invoice.invoiceId)
            .then(function () {
                NotificationService.logSuccess("Procesada..!");
                $state.reload();
            }, function () {
                NotificationService.logError("Algo salio mal.")
            })
    };

    $scope.doEdit = () => {
        if ($scope.invoice.status === 'PROCESSED')
            NotificationService.logError("No puede editar, documento ya ha sido procesado.");
        else $state.go('app.misc.editInvoice', {id: $scope.invoice.invoiceId});

    };
    $scope.getWord = function (t) {
        let text = t;
        if (text === null)
            return "";
        text = text.toLowerCase();
        let wordList = text.split(" ");
        let result = '';
        wordList.forEach(function (word) {
            if (word.length > 5)
                word = word.substring(0, 5);
            result = result + word + " ";
        });
        return result;
    }
});
