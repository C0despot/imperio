angular.module('app.auth')
    .controller('PrinterCtrl', function ($scope, $state, $http, User, NotificationService) {

        $scope.printer = {};
        $scope.user = {};
        $scope.$on('$viewContentLoaded', function () {
            $(".fa-arrow-circle-left").click(function (event) {
                event.preventDefault();
                window.history.back();
            });
            $scope.isLoading++;
            $http.get('user/me')
                .then(function (response) {
                    $scope.user = response.data;
                }, function (error) {
                    NotificationService.logError("Algo Salio mal al cargar la informacion del usuario.")
                })
                .finally(function () {
                    $scope.isLoading--;
                })

            $scope.isLoading++;
            $http.get('http://localhost:60123/service/printer')
                .then(function (response) {
                    let respData = response.data;
                    $scope.printer.directory = respData['printer.path'];
                    $scope.printer.modifier = respData['printer.modifier'];
                    $("#printer-pos").select2('data', {id: respData['printer.pos'], text: respData['printer.pos'].toUpperCase()}).trigger('change');
                    $("#printer").select2('data', {id: respData['printer.basic'], text: respData['printer.basic'].toUpperCase()}).trigger('change');
                }, function (error) {
                    NotificationService.logError("Algo Salio mal al cargar la informacion del printer.")
                })
                .finally(function () {
                    $scope.isLoading--;
                })
        });

        $scope.savePrinter = function () {
            $scope.isLoading++;
            $scope.printer.modifier = $scope.user.email;
            $scope.printer.directory = $scope.printer.directory.split('\\').join('/');
            $http.post('http://localhost:60123/service/printer', $scope.printer)
                .then(function (response) {

                }, function (error) {
                    NotificationService.logError("Algo Salio mal al guardar la informacion de impresion.")
                })
                .finally(function () {
                    $scope.isLoading--;
                })
        }
    });
