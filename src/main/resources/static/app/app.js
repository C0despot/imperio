'use strict';

/**
 * @ngdoc overview
 * @name app [smartadminApp]
 * @description
 * # app [smartadminApp]
 *
 * Main module of the application.
 */

angular.module('app', [
    'ngSanitize',
    'ngAnimate',
    'restangular',
    'ui.router',
    'ui.bootstrap',

    // Smartadmin Angular Common Module
    'SmartAdmin',

    // App
    'app.auth',
    'app.layout',
    'app.chat',
    'app.dashboard',
    'app.calendar',
    'app.inbox',
    'app.graphs',
    'app.tables',
    'app.forms',
    'app.ui',
    'app.widgets',
    'app.maps',
    'app.appViews',
    'app.misc',
    'app.smartAdmin',
    'app.eCommerce'
])
    .config(function ($provide, $httpProvider, RestangularProvider, $locationProvider) {

        // $locationProvider.html5Mode(true);
        // Intercept http calls.
        $provide.factory('ErrorHttpInterceptor', function ($q) {
            let errorCounter = 0;

            function notifyError(rejection) {
                $.bigBox({
                    title: rejection.status + ' ' + rejection.statusText,
                    content: rejection.data,
                    color: "#C46A69",
                    icon: "fa fa-warning shake animated",
                    number: ++errorCounter,
                    timeout: 6000
                });
            }

            return {
                // On request failure
                requestError: function (rejection) {
                    // show notification
                    // notifyError(rejection);

                    // Return the promise rejection.
                    return $q.reject(rejection);
                },

                // On response failure
                responseError: function (rejection) {
                    // show notification
                    // notifyError(rejection);
                    // Return the promise rejection.
                    return $q.reject(rejection);
                }
            };
        });

        $provide.factory('sessionInjector', ['$browser', function($browser) {
            return {
                request: function (config) {
                    return config;
                }
            };
        }]);
        // Add the interceptor to the $httpProvider.
        $httpProvider.interceptors.push('ErrorHttpInterceptor');
        $httpProvider.interceptors.push('sessionInjector');

        RestangularProvider.setBaseUrl(location.pathname.replace(/[^\/]+?$/, ''));

    })
    .constant('APP_CONFIG', window.appConfig)

    .run(function ($rootScope, $state, $stateParams, $http, NotificationService) {
        $rootScope.$on('$stateChangeStart',
            function (event, toState, toParams, fromState, fromParams) {
                if (toState.name !== 'login' && toState.name !== 'lock') {
                    $http.get("user")
                        .then(function (response) {
                            localStorage.setItem('session', JSON.stringify(response.data));
                        }, function (error) {
                            $state.go("login");
                        });
                }
            });
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        setInterval(function () {
            let session = localStorage.getItem('session');
            if (session) {
                session = JSON.parse(session);
                let updatedTime = new Date(session.lastAccessedTime);
                let diff = ((new Date()).getTime() - updatedTime.getTime()) / 1000;
                if (diff > session.maxInactiveInterval && $state.current.name !== 'login' && $state.current.name !== 'lock') {
                    $state.go("login");
                    $http.get('logout').then(function () {}, function () {});
                    NotificationService.logError("La Session Ha Expirado");
                }
            }
        }, 1000);
    });
