angular.module('app')
    .factory('NotificationService', function ($http, $rootScope) {
        return {
            logError: function (message) {
                $.smallBox({
                    title: "Error...!!!",
                    content: message,
                    color: "#d8023e",
                    iconSmall: "fa fa-thumbs-down bounce animated",
                    timeout: 4000
                });
            },
            logSuccess: function (message) {
                $.smallBox({
                    title: "Done!",
                    content: message,
                    color: "#4071f7",
                    iconSmall: "fa fa-thumbs-up bounce animated",
                    timeout: 4000
                });
            },
            promptSecure: function (message, confirmCallBack, cancelCallBack) {
                $.SmartMessageBox({
                    title: "Confirmacion!",
                    content: message,
                    buttons: '[No][Si]'
                }, function (ButtonPressed) {
                    if (ButtonPressed === "Si") {
                        confirmCallBack();
                    }
                    if (ButtonPressed === "No") {
                        cancelCallBack();
                    }
                });
            }
        }

    });