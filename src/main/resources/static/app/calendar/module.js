"use strict";


angular
    .module('app.calendar', ['ngResource', 'ui.router'])
    .config(function ($stateProvider) {

        $stateProvider
            .state('app.calendar', {
                url: '/calendar',
                views: {
                    content: {
                        templateUrl: 'app/calendar/views/calendar.tpl.html'
                    }
                },
                data: {
                    title: 'Calendar'
                }
            })
            .state('app.user', {
                url: '/user/new',
                views: {
                    content: {
                        templateUrl: "app/calendar/create/user.html",
                        controller: 'CreateUserCtrl'
                    }
                },
                data: {
                    title: 'Nuevo Usuario'
                }
            })
            .state('app.profile', {
                url: '/user/profile',
                views: {
                    content: {
                        templateUrl: "app/calendar/create/user.html",
                        controller: 'EditUserCtrl'
                    }
                },
                data: {
                    title: 'Informacion Personal'
                }
            });
    });


