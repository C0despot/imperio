angular.module('app.dashboard')
    .controller('CloseSalesModalCtrl', function ($scope, cashBox, $http, SalesService, NotificationService, $uibModalInstance) {

        $scope.isLoading = 0;
        $scope.sqrt = {};
        $scope.isReadOnly = false;
        setTimeout(function () {
            if (cashBox === null) {
                $scope.isLoading++;
                SalesService
                    .currentOpeningCurrency()
                    .then(function (response) {
                        $scope.sqrt = angular.copy(response.data);
                        let a = response.data.openCashBox;
                        $scope.currencySelected = JSON.parse(a.currencySelected);
                        $scope.totalOpenCash = JSON.parse(a.totalAmount);
                    }, function (error) {
                    })
                    .finally(function () {
                        $scope.isLoading--;
                    });
            } else {
                $scope.sqrt = angular.copy(cashBox);
                let a = cashBox.openCashBox;
                let b = cashBox.closeCashBox;
                $scope.currencySelected = JSON.parse(a.currencySelected);
                $scope.totalOpenCash = JSON.parse(a.totalAmount);
                $scope.isReadOnly = true;
                if (b) {
                    $scope.currency = b;
                    $scope.finalCurrencySelected = JSON.parse(b.currencySelected);
                }
            }
        }, 50);

        setInterval(function () {
            let calculated = $scope.sqrt.totalCash ? Number($scope.sqrt.totalCash.replace(/,/g, "")) + $scope.totalOpenCash : $scope.totalOpenCash;
            let reported = $scope.currency.totalAmount;

            $scope.isValidSqrt = (calculated && calculated >= reported - 10 && calculated <= reported + 10);
        }, 100);

        $scope.uibModalInstance = $uibModalInstance;

        $scope.currencySelected = {};
        $scope.finalCurrencySelected = {
            one: {
                value: 1,
                total: 0
            },
            five: {
                value: 5,
                total: 0
            },
            ten: {
                value: 10,
                total: 0
            },
            twentyFive: {
                value: 25,
                total: 0
            },
            fifty: {
                value: 50,
                total: 0
            },
            oneHundred: {
                value: 100,
                total: 0
            },
            twoHundred: {
                value: 200,
                total: 0
            },
            fiveHundred: {
                value: 500,
                total: 0
            },
            oneThousand: {
                value: 1000,
                total: 0
            },
            twoThousand: {
                value: 2000,
                total: 0
            }
        };

        $scope.currency = {
            currencySelected: JSON.stringify($scope.finalCurrencySelected),
            totalAmount: 0.0,
            date: new Date(),
        };

        $scope.tab = 0;
        $scope.previous = function () {
            if ($scope.tab > 0)
                $scope.tab--;
        };

        $scope.next = function () {
            if ($scope.tab === 2) {
                NotificationService.promptSecure("Seguro que desea continuar con el cierre de caja?", function () {
                    $scope.isLoading++;
                    SalesService
                        .sqrtCloseStep($scope.currency)
                        .then(function (response) {
                            $scope.sqrt.closeCashBox = response.data;
                            $scope.isLoading++;
                            SalesService
                                .processCashBox($scope.sqrt)
                                .then(function (response) {
                                    $uibModalInstance.close();
                                }, function (error) {

                                })
                                .finally(function () {
                                    $scope.isLoading--;
                                })
                        }, function (error) {

                        })
                        .finally(function () {
                            $scope.isLoading--;
                        });
                }, function () {
                })
            }
            if ($scope.tab < 2)
                $scope.tab++;

        };

        $scope.totalOpenCash = 0.0;
        $scope.process = function () {
            NotificationService
                .promptSecure(`Confirma que el total: ${$scope.currency.totalAmount} es correcto?`, function () {
                    SalesService
                        .sqrtOpenStep($scope.currency)
                        .then(function (response) {
                            let a = response.data.openCashBox;
                            $scope.currencySelected = JSON.parse(a.currencySelected);
                            $scope.totalOpenCash = JSON.parse(a.totalAmount);
                        }, function (error) {

                        });
                }, function () {
                })
        };

        $scope.updateEntries = function () {
            $scope.currency.currencySelected = JSON.stringify($scope.finalCurrencySelected);
            let total = 0;
            let currency = $scope.finalCurrencySelected;
            for (let prop in currency) {
                let fieldObj = currency[prop];
                if (!fieldObj.total)
                    fieldObj.total = 0.0;
                total += fieldObj.value * fieldObj.total;
            }
            $scope.currency.totalAmount = total;
        };

    });
