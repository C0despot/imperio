angular.module('app.dashboard')
    .controller('OpenDayModalCtrl', function ($scope, $http, SalesService, NotificationService, $uibModalInstance) {

        $scope.uibModalInstance = $uibModalInstance;
        $scope.isLoading = 0;
        $scope.currencySelected = {
            one: {
                value: 1,
                total: 0
            },
            five: {
                value: 5,
                total: 0
            },
            ten: {
                value: 10,
                total: 0
            },
            twentyFive: {
                value: 25,
                total: 0
            },
            fifty: {
                value: 50,
                total: 0
            },
            oneHundred: {
                value: 100,
                total: 0
            },
            twoHundred: {
                value: 200,
                total: 0
            },
            fiveHundred: {
                value: 500,
                total: 0
            },
            oneThousand: {
                value: 1000,
                total: 0
            },
            twoThousand: {
                value: 2000,
                total: 0
            }
        };

        $scope.currency = {
            currencySelected: JSON.stringify($scope.currencySelected),
            totalAmount: 0.0,
            date: new Date(),
        };

        $scope.process = function () {
            NotificationService
                .promptSecure(`Confirma que el total: ${$scope.currency.totalAmount} es correcto?`, function () {
                    $scope.isLoading++;
                    SalesService
                        .sqrtOpenStep($scope.currency)
                        .then(function (response) {
                            $uibModalInstance.close();
                        }, function (error) {
                            NotificationService.logError("Algo salio mal en el momento de procesar el documento")
                        })
                        .finally(function () {
                            $scope.isLoading--;
                        });
                }, function () {
                })
        };

        $scope.updateEntries = function () {
            $scope.currency.currencySelected = JSON.stringify($scope.currencySelected);
            let total = 0;
            let currency = $scope.currencySelected;
            for (let prop in currency) {
                let fieldObj = currency[prop];
                if (!fieldObj.total)
                    fieldObj.total = 0.0;
                total += fieldObj.value * fieldObj.total;
            }
            $scope.currency.totalAmount = total;
        };
    });