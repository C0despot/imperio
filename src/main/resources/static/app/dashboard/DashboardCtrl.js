'use strict';

angular.module('app.dashboard').controller('DashboardCtrl', function ($scope, $uibModal, $interval, SalesService, ProductService) {

    $scope.$on('$viewContentLoaded', function () {
        ProductService
            .validateDiscount()
            .then(function (response) {
            }, function (error) {
                $(".admin").hide();
            });
        SalesService
            .sqrtNextStep()
            .then(function (response) {
                if (response.data.days > 0) {
                    $scope.closeSales(true);
                }
            }, function (error) {
                $scope.openSales();
            })
    });

    $scope.openSales = function () {
        $uibModal.open({
            controller: 'OpenDayModalCtrl',
            templateUrl: 'app/dashboard/sqrt/open.html',
            backdrop: 'static',
            keyboard: false,
            resolve: {
                isNew: function () {
                    return true;
                },
                isEntry: function () {
                    return false;
                }
            }
        });
    };

    $scope.closeSales = function (mandatory) {
        let modalInstance = $uibModal.open({
            controller: 'CloseSalesModalCtrl',
            templateUrl: 'app/dashboard/sqrt/close.html',
            backdrop: 'static',
            keyboard: false,
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.datatables.js',
                        'build/vendor.ui.js'
                    ]);

                },
                cashBox: function () {
                    return null;
                }
            }
        });

        modalInstance.result.then(function (resp) {
            if (mandatory)
                $scope.openSales();
        })
    };

});