(function () {
    'use strict';
    angular.module('app.eCommerce')
        .service('ProductService', ProductService);
})();

ProductService.$inject = ['$http'];

function ProductService($http) {

    return {
        productUrl: 'api/product',
        utilUrl: 'api/util',
        getProducts: function (page) {
            return $http.get(this.productUrl + "/all" + "?size=10&page=" + page);
        },
        getCategory: function () {
            return $http.get(this.utilUrl + "/category/all");
        },
        getStatus: function () {
            return $http.get(this.utilUrl + "/status/all");
        },
        getPrice: function () {
            return $http.get(this.utilUrl + "/price/all");
        },
        getBrand: function () {
            return $http.get(this.utilUrl + "/brand/all");
        },
        getVendor: function () {
            return $http.get(this.utilUrl + "/vendor/all");
        },
        getPack: function () {
            return $http.get(this.utilUrl + "/pack/all");
        },
        aggregate: function (object) {
            return $http.post(this.utilUrl, object);
        },
        saveProduct: function (product) {
            return $http.post(this.productUrl + "/save", product);
        },
        getOne: function (productId) {
            return $http.get(this.productUrl + '/one/' + productId);
        },
        saveProductImage: function (image, product) {
            let fd = new FormData();
            fd.append('file', image);
            return $http.post(this.productUrl + '/image/' + product, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
        },
        getByFilter: function (page, queryString) {
            return $http.get(this.productUrl + "/search" + "?size=10&page=" + page + queryString);
        },
        saveInvoice: function (invoice) {
            return $http.post('api/sales', invoice);
        },
        getCustomer: function (id) {
            return $http.get(this.utilUrl + '/customer?term=' + id);
        },
        validateDiscount: function () {
            return $http.get(this.utilUrl + "/authorities");
        },
        sum: function (items, prop) {
            return items.reduce(function (a, b) {
                let total = b ['qty'].toString().replace(/,/g, "");
                let totalB = b[prop].toString().replace(/,/g, "");
                let totalA = a.toString().replace(/,/g, "");
                return totalA + totalB * total;
            }, 0);
        },
        processReceipt: function (doc) {
            return $http.post(this.productUrl + "/receipt/document", doc);
        },
        getReceiptDocument: function (page, queryString) {
            return $http.get(this.productUrl + "/receipt/document/filter?size=10&page=" + page + queryString);
        },
        getSingleReceiptDocument: function (product) {
            return $http.get(this.productUrl + `/receipt/document/${product}/extended`);
        }
    }
}