"use strict";


angular.module('app.eCommerce', ['ui.router'])
    .config(function ($stateProvider) {

        $stateProvider
            .state('app.eCommerce', {
                abstract: true,
                data: {
                    title: 'E-Commerce'
                }
            })

            .state('app.eCommerce.orders', {
                url: '/products',
                data: {
                    title: 'All Products'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/e-commerce/views/all.product.html',
                        controller: 'ProductsCtrl',
                        resolve: {
                            orders: function($http, APP_CONFIG){
                                return $http.get(APP_CONFIG.apiRootUrl + '/e-commerce/orders.json')
                            }
                        }
                    }
                },
                resolve: {
                    scripts: function(lazyScript){
                        return lazyScript.register([
                            'build/vendor.datatables.js',
                            'build/vendor.ui.js'
                        ]);

                    }
                }
            })
            .state('app.eCommerce.new', {
                url: '/products/new',
                data: {
                    title: 'New Product'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/e-commerce/views/product.html',
                        controller: 'NewProductCtrl'
                    }
                }
            })
            .state('app.eCommerce.edit', {
                url: '/products/:id/edition',
                data: {
                    title: 'Edit Product'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/e-commerce/views/product.html',
                        controller: 'EditProductCtrl'
                    }
                }
            })
            .state('app.eCommerce.details', {
                url: '/products/:id/details',
                data: {
                    title:  'Product Details'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/e-commerce/views/detail.html',
                        controller: 'ProductDetailCtrl'
                    }
                }
            })

            .state('app.eCommerce.products', {
                url: '/e-commerce/products-view',
                data: {
                    title: 'Products View'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/e-commerce/views/products.html'
                    }
                }
            })

            .state('app.eCommerce.detail', {
                url: '/e-commerce/products-detail',
                data: {
                    title: 'Products Detail'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/e-commerce/views/detail.html'
                    }
                }
            })
    });
