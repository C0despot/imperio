angular.module('app.eCommerce').controller('ProductsCtrl', function ($scope, ProductService, NotificationService) {

    $scope.isLoading = 0;
    $scope.pager = {
        index: 1,
        totalPages: 1
    };
    $scope.productList = [];
    $scope.totalPages = [];

    $scope.setPager = function (value) {
        if(value > $scope.pager.totalPages )
            value = $scope.pager.totalPages;
        else if(value < 1)
            value = 1;
        else if (typeof value === 'undefined') {
            value = $scope.pager.totalPages;
        }
        $scope.pager.index = value;
        $scope.pageHelper = value;
        $scope.isLoading++;
        $scope.getByPageAndFilter();
    };
    $(".dropdown-menu a").click(function() {
        $(this).closest(".dropdown-menu").prev().dropdown("toggle");
    });
    $scope.$on('$viewContentLoaded', function(){
        $scope.isLoading++;
        ProductService.getProducts($scope.pager.index - 1)
            .then(function (response) {
                $scope.productList = angular.copy(response.data.content);
                $scope.pager.totalPages = response.data.totalPages;
            }, function (error) {
                NotificationService.logError("Hubo un problema cargando los productos")
            })
            .finally(function () {
                $scope.isLoading--;
            });
    });

    $scope.searchObject = {
        name : 'Nombre',
        description: 'Descripcion',
        category: 'Categoria'
    };
    $scope.searchBy = 'name';
    $scope.criteria = '';

    $scope.applyFilters = function () {
        $scope.isLoading++;
        $scope.pager.index = 1;
        $scope.pageHelper = 1;
        $scope.getByPageAndFilter()
    };

    $scope.getByPageAndFilter = function () {
        let filterUrl = '&searchBy='+$scope.searchBy + "&criteria="+ $scope.criteria;
        ProductService.getByFilter($scope.pager.index - 1, filterUrl)
            .then(function (response) {
                $scope.productList = response.data.content;
                $scope.pager.totalPages = response.data.totalPages;
            }, function (error) {
                NotificationService.logError("La busqueda no trajo resultados.")
            })
            .finally(function () {
                $scope.isLoading--;
            });
    }


});