angular
    .module('app.eCommerce')
    .controller('NewProductCtrl', function ($scope, $state, ProductService, NotificationService) {

        $scope.statusList = [];
        $scope.brandList = [];
        $scope.priceList = [];
        $scope.packList = [];
        $scope.categoryList = [];
        $scope.vendorList = [];
        $scope.isEditing = false;
        $scope.image = null;
        $scope.isLoading = 0;
        $scope.aggregation = {
            index: null,
            description: null
        };

        $scope.product = {
            salePrice: 0,
            purchasePrice: 0,
            gain: 30,
            wholesaleQuantity: 0,
            wholesalePrice: 0,
            wholesalePercent: 0,
            allowTax: 'Y',
            priceType: {
                id: 1000000000,
                name: "PORCENTAJE"
            }
        };

        $scope.$on('$viewContentLoaded', function () {
            $scope.isLoading++;
            $scope.loadDependencies();
        });

        $scope.loadDependencies = function () {
            ProductService.getStatus()
                .then(function (response) {
                    $scope.statusList = angular.copy(response.data)
                });
            ProductService.getPack()
                .then(function (response) {
                    $scope.packList = angular.copy(response.data)
                });
            ProductService.getCategory()
                .then(function (response) {
                    $scope.categoryList = angular.copy(response.data)
                });
            ProductService.getVendor()
                .then(function (response) {
                    $scope.vendorList = angular.copy(response.data)
                });
            ProductService.getPrice()
                .then(function (response) {
                    $scope.priceList = angular.copy(response.data);

                    $scope.priceList.forEach(function (price) {
                        if (price.id === 1000000000)
                            $scope.product.priceType = price;
                    });
                });
            ProductService.getBrand()
                .then(function (response) {
                    $scope.brandList = angular.copy(response.data)
                })
                .finally(function () {
                    $scope.isLoading--;
                });
        }

        $scope.addNew = function () {
            $scope.isLoading++;
            ProductService.aggregate($scope.aggregation)
                .then(function (response) {
                    $scope.loadDependencies();
                })
                .finally(function () {
                    $scope.isLoading--;
                })
        };

        $scope.setSalePrice = function () {
            if ($scope.product.priceType.id === 1000000001) {
                $scope.product.salePrice = Number(($scope.product.purchasePrice.toString().replace(/,/g, "") * (1 + $scope.product.gain / 100)).toFixed(2));
            } else if ($scope.product.priceType.id === 1000000002) {
                $scope.product.salePrice = Number((Number($scope.product.purchasePrice.toString().replace(/,/g, "")) + Number($scope.product.gain)).toFixed(2));
            }
            $scope.product.wholesalePrice = Number($scope.product.salePrice * $scope.product.wholesaleQuantity * (1 - $scope.product.wholesalePercent / 100).toFixed(2));
        };

        $scope.saveProduct = function () {
            $scope.isLoading++;
            $scope.product.purchasePrice = $scope.product.purchasePrice.toString().replace(/,/g, "");
            $scope.product.salePrice = $scope.product.salePrice.toString().replace(/,/g, "");
            $scope.product.totalAmount = $scope.product.totalAmount.toString().replace(/,/g, "");
            ProductService.saveProduct($scope.product)
                .then(function (response) {
                    if ($scope.image === null) {
                        $state.go('app.eCommerce.details', {id: response.data.id});
                        NotificationService.logSuccess("Cambios guardados.");
                    }
                    else {
                        $scope.isLoading++;
                        ProductService.saveProductImage($scope.image, response.data.id)
                            .then(function (response) {
                                $state.go('app.eCommerce.details', {id: response.data.id});
                                NotificationService.logSuccess("Cambios guardados.");
                            }, function (data) {
                                NotificationService.logError("Problemas al subir la imagen");
                            })
                            .finally(function () {
                                $scope.isLoading--;
                            });
                    }
                }, function (error) {
                    NotificationService.logError("ALGO SALIO MAL. INTENTELO NUEVAMENTE");
                })
                .finally(function () {
                    $scope.isLoading--;
                });
        };
    });