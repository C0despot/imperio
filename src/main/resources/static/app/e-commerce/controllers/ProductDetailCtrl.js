angular
    .module('app.eCommerce')
    .controller('ProductDetailCtrl', function ($scope, ProductService, NotificationService, $stateParams, $state) {

        $scope.isLoading = 0;
        $scope.$on('$viewContentLoaded', function () {
            $scope.isLoading++;
            ProductService.getOne($stateParams.id)
                .then(function (response) {
                    $scope.isLoading--;
                    $scope.product = angular.copy(response.data);
                })
        });
    });
