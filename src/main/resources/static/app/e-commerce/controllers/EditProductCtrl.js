angular
    .module('app.eCommerce')
    .controller('EditProductCtrl', function ($scope, ProductService, NotificationService, $stateParams, $state) {

        $scope.product = {};
        $scope.statusList = [];
        $scope.brandList = [];
        $scope.priceList = [];
        $scope.packList = [];
        $scope.categoryList = [];
        $scope.vendorList = [];
        $scope.isEditing = true;
        $scope.image = null;
        $scope.isLoading = 0;

        $scope.$on('$viewContentLoaded', function () {
            $scope.isLoading++;
            $scope.loadDependencies();
            ProductService.getOne($stateParams.id)
                .then(function (response) {
                    $scope.product = angular.copy(response.data);
                    $scope.populateSelects(angular.copy($scope.product));
                })
                .finally(function () {
                    $scope.isLoading--;
                })
        });
        $scope.setSalePrice = function () {
            if ($scope.product.priceType.id === 1000000001) {
                $scope.product.salePrice = Number(($scope.product.purchasePrice.toString().replace(/,/g, "") * (1 + $scope.product.gain / 100)).toFixed(2));
            } else if ($scope.product.priceType.id === 1000000002) {
                $scope.product.salePrice = Number((Number($scope.product.purchasePrice.toString().replace(/,/g, "")) + Number($scope.product.gain)).toFixed(2));
            }
            $scope.product.wholesalePrice = Number($scope.product.salePrice * $scope.product.wholesaleQuantity * (1 - $scope.product.wholesalePercent / 100).toFixed(2));
        };
        $scope.addNew = function () {
            $scope.isLoading++;
            ProductService.aggregate($scope.aggregation)
                .then(function (response) {
                    $scope.loadDependencies();
                })
                .finally(function () {
                    $scope.isLoading--;
                })
        };
        $scope.populateSelects = function (product) {
            $scope.vendorList.forEach(function (t) {
                if (product.vendor && t.id === product.vendor.id) {
                    $scope.product.vendor = t;
                }
            });
            $scope.brandList.forEach(function (t) {
                if (product.brand && t.id === product.brand.id) {
                    $scope.product.brand = t;
                }
            });
            $scope.categoryList.forEach(function (t) {
                if (product.category && t.id === product.category.id) {
                    $scope.product.category = t;
                }
            });
            $scope.packList.forEach(function (t) {
                if (product.pack && t.id === product.pack.id) {
                    $scope.product.pack = t;
                }
            })
            $scope.priceList.forEach(function (t) {
                if (product.priceType && t.id === product.priceType.id) {
                    $scope.product.priceType = t;
                }
            })
        };
        $scope.loadDependencies = function () {
            ProductService.getStatus()
                .then(function (response) {
                    $scope.statusList = angular.copy(response.data)
                });
            ProductService.getPack()
                .then(function (response) {
                    $scope.packList = angular.copy(response.data)
                });
            ProductService.getCategory()
                .then(function (response) {
                    $scope.categoryList = angular.copy(response.data)
                });
            ProductService.getVendor()
                .then(function (response) {
                    $scope.vendorList = angular.copy(response.data);
                });
            ProductService.getPrice()
                .then(function (response) {
                    $scope.priceList = angular.copy(response.data)
                });
            ProductService.getBrand()
                .then(function (response) {
                    $scope.brandList = angular.copy(response.data)
                });
        }

        $scope.saveProduct = function () {
            $scope.isLoading++;
            $scope.product.purchasePrice = $scope.product.purchasePrice.toString().replace(/,/g, "");
            $scope.product.salePrice = $scope.product.salePrice.toString().replace(/,/g, "");
            $scope.product.totalAmount = $scope.product.totalAmount.toString().replace(/,/g, "");
            ProductService.saveProduct($scope.product)
                .then(function (response) {
                    if ($scope.image === null) {
                        $state.go('app.eCommerce.details', {id: response.data.id});
                        NotificationService.logSuccess("Cambios guardados.");
                    }
                    else {
                        $scope.isLoading++;
                        ProductService.saveProductImage($scope.image, response.data.id)
                            .then(function (response) {
                                $state.go('app.eCommerce.details', {id: response.data.id});
                                NotificationService.logSuccess("Cambios guardados.");
                            }, function (data) {
                                NotificationService.logError("Problemas al subir la imagen");
                            })
                            .finally(function () {
                                $scope.isLoading--;
                            });
                    }
                }, function (error) {
                    NotificationService.logError("ALGO SALIO MAL. INTENTELO NUEVAMENTE");
                })
                .finally(function () {
                    $scope.isLoading--;
                })
        };
    });